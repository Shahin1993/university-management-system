<?php include("header.php"); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Save Course</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6">
            <div class="well">


                <form>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Code</label>
                        <input type="text" class="form-control" id="inputSuccess">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Name</label>
                        <input type="text" class="form-control" id="inputSuccess">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Credit</label>
                        <input type="text" class="form-control" id="inputSuccess">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Description</label>
                        <textarea type="text" class="form-control" id="inputSuccess" cols="12" rows="5"></textarea>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" >Department</label>
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label">Semester</label>
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-lg btn-success">Save</button>

                </form>




                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /.row -->
<?php include("footer.php"); ?>
