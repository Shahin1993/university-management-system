<?php include("header.php"); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Student Result Form</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6">
            <div class="well">


                <form>
                    <div class="form-group has-success">
                        <label class="control-label" >Student Reg No</label>
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Name</label>
                        <input type="text" class="form-control" id="inputSuccess">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Email</label>
                        <input type="text" class="form-control" id="inputSuccess">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Department</label>
                        <input type="text" class="form-control" id="inputSuccess">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" >Select Course</label>
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label">Select Grade Letter</label>
                        <select class="form-control">
                            <option>Select</option>
                            <option>A+</option>
                            <option>A</option>
                            <option>A-</option>
                            <option>B+</option>
                            <option>B</option>
                            <option>B-</option>
                            <option>C+</option>
                            <option>C</option>
                            <option>C-</option>
                            <option>D+</option>
                            <option>D</option>
                            <option>D-</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-lg btn-success">Save</button>

                </form>




                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /.row -->
<?php include("footer.php"); ?>
