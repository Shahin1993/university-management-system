<?php

 namespace App\allocateClass;
 use App\database\database;
 session_start();
class allocateClass
{

    private $department_id;
    private $course_id;
    private $room_id;
    private $day_id;
    private $start_time;
    private $end_time;

    public function addAlloctecourse($data1 = '')
    {

        $this->department_id = $data1['department_id'];
        $this->course_id = $data1['course_id'];
        $this->room_id = $data1['room_id'];
        $this->day_id = $data1['day_id'];
        $this->start_time = $data1['start_time'];
        $this->end_time = $data1['end_time'];


        return $this;
    }
    public function storeAllocateCourse()
    {
        if ($_POST) {
            if (empty($this->department_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Please Select a department name..</div>";
                header("location: ../pages/allocate_classrooms.php");
            } elseif (empty($this->course_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Course name cannot be empty</div>";
                header("location: ../pages/allocate_classrooms.php");
            } elseif (empty($this->room_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Please ekta room select koron;</div>";
                header("location: ../pages/allocate_classrooms.php");
            } elseif (empty($this->day_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Please 7 diner ek din to select korben?/</div>";
                header("location: ../pages/allocate_classrooms.php");
            } elseif (empty($this->start_time)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Please insert start time..</div>";
                header("location: ../pages/allocate_classrooms.php");
            } elseif (empty($this->end_time)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'> Please insert end time ..</div>";
                header("location: ../pages/allocate_classrooms.php");
            }
//            elseif ($this->c_credit<.5 or $this->c_credit>5) {
//                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Credit must be 0.5 to 5 </div>";
//            }

            else {
                $db = database::getInstance();
                $statement = $db->prepare("SELECT * FROM course_class_allocate WHERE start_time=?");
                $statement->execute(array($this->start_time));
                $total = $statement->rowCount();


                if ($total > 0) {
                    $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px'>This start time is already a course allocated</div>";
                    header("location: ../pages/allocate_classrooms.php");
                } else {
                    $db = database::getInstance();
                    $stmt = $db->prepare("SELECT * FROM course_class_allocate WHERE end_time=?");
                    $stmt->execute(array($this->end_time));
                    $count = $stmt->rowCount();

                    if ($count > 0) {
                        $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px'>This end time is already a course allocated!!</div>";
                        header("location: ../pages/allocate_classrooms.php");
                    } else {

                        $db = database::getInstance();
                        $sql = "INSERT INTO course_class_allocate(department_id,course_id,room_id,day_id,start_time,end_time,created_at) values(:dpt, :crd, :rm, :day, :str, :end, :d)";
                        $stmt = $db->prepare($sql);
                        $status = $stmt->execute(
                            array(
                                ':dpt' => $this->department_id,
                                ':crd' => $this->course_id,
                                ':rm' => $this->room_id,
                                ':day' => $this->day_id,
                                ':str' => $this->start_time,

                                ':end' => $this->end_time,
                                ':d' => date('Y-m-d'),
                            ));
                        if ($status) {
                            $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size: 30px;'>Allocat Class roome  successfully.</div>";

                            header("location: ../pages/allocate_classrooms.php");
                        } else {
                            echo "something is wrong";
                        }
                    }
                }

            }
        }
       }
//
//    }
}
