<?php
include ('../vendor/autoload.php');
use App\student\student;
use App\database\database;

$obj=new student();
$row=$obj->viewAllstudent();
?>

    <?php include("header.php"); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-9">
                    <h1 class="page-header">View All Student</h1>
                </div>
                <div class="col-lg-3">
                    <h1 class="page-header"><a href="student_register.php">Add Student</a> </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <?php
                        //session_start();
                        if(isset($_SESSION['Message'])){
                            echo $_SESSION['Message'];
                            unset ($_SESSION['Message']);
                        }
                        ?>
                        <?php
//                    $db = new PDO('mysql:host=localhost;dbname=lebs', 'root', '');
//                    $query =" SELECT * ,concat(LPAD(st_department,3,'0') ,'-', YEAR(NOW())) as stud_id FROM students";
//                    $stmt = $db->prepare($query);
//                    $stmt->execute(array($_GET['id']));
//                    $data = $stmt->fetch();
//                    //            echo "<pre>";
//                    //            print_r($data);
//                    //   $i=0;         echo "</pre";
//
//                        echo $data['stud_id'];
//

                        ?>

                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover text-center">
                                    <thead>
                                        <tr style="background-color:steelblue;color: white;font-weight: bold;font-size:20px; ">
                                            <td>No</td>
                                            <td>Student ID</td>
                                            <td>Name</td>
                                            <td>Email</td>
                                            <td>Contact NO</td>
                                            <td>Date</td>
                                            <td>Department</td>
                                            <td>Action</td>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=0;
                                    foreach ($row as $stu)

                                    {
                                        $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $stu['stu_id']; ?></td>
                                            <td><?php echo $stu['st_name']; ?></td>
                                            <td><?php echo $stu['st_email']; ?></td>
                                            <td><?php echo $stu['st_contact']; ?></td>
                                            <td><?php echo $stu['date']; ?></td>
                                            <td>

                                                <?php
                                               // include ('config.php');

                                                //$db = new PDO('mysql:host=localhost;dbname=lebs', 'root', '');
                                                $db = database::getInstance();
                                                $statement= $db->prepare("SELECT * FROM departments WHERE d_code=?");
                                                $statement->execute(array($stu['st_department']) );
                                                $result= $statement->fetchAll(PDO::FETCH_ASSOC);
                                                foreach ($result as $dpt)
                                                {
                                                    echo $dpt['department'];
                                                }
                                                ?>
                                            <td>
                                            <a href="student_edit.php?id=<?php echo $stu['id']; ?>">Edit</a>|
                                                &nbsp;<a onclick= 'return confirmDelete();' href= "softDeleteStudent.php?id=<?php echo $stu['id']; ?>" >
                                                    Delete</a></td>
                                            </td>

                                        </tr>
                                    <?php
                                    } ?>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->

                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        </div>
    <?php include("footer.php"); ?>



