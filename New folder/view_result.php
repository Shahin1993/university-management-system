<?php
include ('../vendor/autoload.php');
use App\department\department;

$obj=new department();
$row=$obj->getDepartment();
?>

    <?php include("header.php"); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View All Departments</h1>
                    <h3><a href="department.php">Add Department</a> </h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <?php
                        //session_start();
                        if(isset($_SESSION['Message'])){
                            echo $_SESSION['Message'];
                            unset ($_SESSION['Message']);
                        }
                        ?>

                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover text-center">
                                    <thead>
                                        <tr style="background-color:steelblue;color: white;font-weight: bold;font-size:20px; ">
                                            <td>No</td>
                                            <td>D-Code</td>
                                            <td>Department Name</td>
                                            <td>Action</td>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=0;
                                    foreach ($row as $dpt)

                                    {
                                        $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $dpt['d_code']; ?></td>
                                            <td><?php echo $dpt['department']; ?></td>
                                            <td>
                                            <a href="edit_department.php?id=<?php echo $dpt['id']; ?>">Edit</a>|
                                                &nbsp;<a onclick= 'return confirmDelete();' href= "delete.php?id=<?php echo $dpt['id']; ?>" >
                                                    Delete</a></td>
                                            </td>

                                        </tr>
                                    <?php
                                    } ?>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->

                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        </div>
    <?php include("footer.php"); ?>



