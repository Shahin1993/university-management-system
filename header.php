<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>lebs coding</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <ul class="nav navbar-nav navbar-left">
                    <li>  <img src="image/logo.png" width="60" height="50"> </li>
                </ul>
                <a class="navbar-brand" href="index.php">LEBS CODING</a>

            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="pages/view_student.php">Student</a></li>
                    <li><a href="pages/view_course.php">Course</a></li>
                    <li><a href="pages/view_department.php">Department</a></li>
                    <li><a href="pages/view_teacher.php">Teacher</a></li>

                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php
                      session_start();
                    if(empty($_SESSION['userid']))

                    { ?>

                        <li><a href="pages/login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    <?php } ?>

                    <?php
                    if(!empty($_SESSION['userid']))
                    { ?>
                        <li><a href="pages/logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                    <?php } ?>

                </ul>
            </div>
        </div>
        <!-- /.navbar-header -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Deparment<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="pages/department.php">Save Department</a>
                            </li>
                            <li>
                                <a href="pages/view_department.php">View Department</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-book fa-fw"></i>Courses<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="pages/course.php">Save Course</a>
                            </li>
                            <li>
                                <a href="pages/view_ajaxCourse.php">View Course</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-edit fa-user-md"></i>Teachers<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="pages/teacher.php">Save Teacher</a>
                            </li>
                            <li>
                                <a href="pages/view_ajaxTeacher.php">View Teacher</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>


                    <li>
                        <a href="#"><i class="fa fa-edit fa-user-md"></i>Course Assign To Teachers<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="pages/courseAssign.php">Course Asign To Teaher</a>
                            </li>
                            <li>
                                <a href="pages/view_course_statics.php">View Course Statics</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bank fa-fw"></i>Class Room<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="pages/allocate_classrooms.php">Allocate Class Room</a>
                            </li>
                            <li>
                                <a href="pages/view_allocated_room.php">View Class Scheule To Room</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-graduation-cap fa-fw"></i>Student<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="pages/student_register.php">Student Registation Form</a>
                            </li>
                            <li>
                                <a href="pages/view_ajaxstudent.php">View All Student</a>
                            </li>
                            <li>
                                <a href="pages/enroll_course.php">Enroll in a Course</a>
                            </li>
                            <li>
                                <a href="pages/add_student_result.php">Save Student Result</a>
                            </li>
                            <li>
                                <a href="pages/view_result.php">View Sudent Result</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-calendar-times-o fa-fw"></i>Restart<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="pages/unallocate.php">UnAllocate Class Room</a> </li>
                            <li><a href="pages/unassign.php">Unassign All Course</a> </li>
                        </ul>
                    <li>
                        <a href="#"><i class="fa fa-files-o fa-fw"></i> Admin Panel<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <?php

                            if(empty($_SESSION['userid']))

                            { ?>

                                <li><a href="pages/login.php">Login</a></li>

                            <?php } else{ ?>
                                <li><a href="pages/logout.php">Logout</a></li>

                            <?php } ?>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>
