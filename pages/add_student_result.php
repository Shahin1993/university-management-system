<?php
error_reporting(0);
include("../vendor/autoload.php");
use App\student\student;
//use App\course\course;
use App\room_day_grade\rdg;

$obj=new student();
$stu=$obj->showStudent_id();

//$obj2=new course();
//$row2=$obj2->viewAllcourse();

$obj3=new rdg();
$grade=$obj3->getGrade();

?>


<?php include("header.php"); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-6">
            <h2 class="page-header">Save student Result</h2>
        </div>
        <div class="col-lg-6">
            <h2 class="page-header"><a href="view_result.php">View Result</a> </h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script>

        $(document).ready(function(){

            $("#sel_depart").change(function(){
                var deptid = $(this).val();

                $.ajax({
                    url: '../store/ajax_addResult.php',
                    type: 'post',
                    data: {depart:deptid},
                    dataType: 'json',
                    success:function(response){


                        var crn = response['student1']['st_name'];
                        var cr=response['student1']['st_email'];
                        var dpt=response['student1']['department'];
                        $("#stname").val(crn)
                        $("#stemail").val(cr)
                        $("#stdpt").val(dpt)

                        var slen = response['course'].length;

                        $("#sel_all").empty();
                        $("#sel_all").append ("<option value=''>"+'Select Course'+"</option>");
                        for (var i = 0; i < slen; i++) {


                            var cd = response['course'][i]['id'];
                            var cn = response['course'][i]['c_name'];

                            $("#sel_all").append ("<option value='"+cd+"'>"+cn+"</option>");

                        }
                    }
                });


            });

        });


    </script>

    <div class="row">
        <div class="col-lg-6">
            <div class="well">
                <?php
                //session_start();
                if(isset($_SESSION['Message'])){
                    echo $_SESSION['Message'];
                    unset ($_SESSION['Message']);
                }
                ?>

                <form action="../store/get_studentResult.php" method="post">
                    <div class="form-group has-success">
                        <label class="control-label" >Student Reg No</label>
                        <select class="form-control" id="sel_depart" name="stu_id">
                            <option value="">Select a student registation number</option>
                            <?php
                            foreach ($stu as $stid)
                            {   ?>
                                <option value="<?php echo $stid['id']; ?>"><?php echo $stid['stu_id']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Name</label>
                        <input type="text" class="form-control" id="stname">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Email</label>
                        <input type="text" class="form-control" id="stemail">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Department</label>
                        <input type="text" class="form-control" id="stdpt">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" >Select Course</label>
                        <select class="form-control" name="course_id" id="sel_all">
                            <option value="">Select a course</option>

                        </select>
                    </div>
                    <div class="form-group  has-success">
                        <label  class="control-label" for="inputSuccess">Select Grade</label>
                        <select class="form-control" name="grade_id">
                            <option value="">Select  Grade</option>
                            <?php
                            foreach ($grade as $grd)
                            {   ?>
                                <option value="<?php echo $grd['grade']; ?>"><?php echo $grd['grade']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-lg btn-success">Save Result</button>

                </form>




                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /.row -->
<?php include("footer.php"); ?>
