<?php
error_reporting(0);
include("../vendor/autoload.php");
use App\student\student;
use App\course\course;
use App\room_day_grade\rdg;

$obj=new student();
$stu=$obj->showStudent_id();

$obj2=new course();
$row2=$obj2->viewAllcourse();

$obj3=new rdg();
$grade=$obj3->getGrade();

?>


<?php include("header.php"); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-6">
            <h2 class="page-header">View Result</h2>
        </div>
        <div class="col-lg-6">
            <h2 class="page-header"><a href="add_student_result.php">Add result</a> </h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script>

        $(document).ready(function(){

            $("#sel_depart").change(function(){
                var deptid = $(this).val();

                $.ajax({
                    url: '../store/ajax_result.php',
                    type: 'post',
                    data: {depart:deptid},
                    dataType: 'json',
                    success:function(response) {


                        var crn = response['student']['st_name'];
                        var cr = response['student']['st_email'];
                        var dpt = response['student']['department'];
                        $("#stname").val(crn)
                        $("#stemail").val(cr)
                        $("#stdpt").val(dpt)


                        var slen = response['grade'].length;

                        $("#sel_all").empty();
                           k=1;
                        for (var i = 0; i < slen; i++) {

                            //  var id = response[i]['id'];
                           // var code = response['grade'][i]['course_id'];
                            var cd = response['grade'][i]['c_code'];
                            var cn = response['grade'][i]['c_name'];
                            var grd = response['grade'][i]['grade_id'];
                            $("#sel_all").append("<tr><td>" + k++ + "</td><td>" + cd+   "</td><td>" + cn + "</td><td>" + grd + "</td></tr>")

                        }
                    }
                });


            });

        });


    </script>
    <?php
    //session_start();
    if(isset($_SESSION['Message'])){
        echo $_SESSION['Message'];
        unset ($_SESSION['Message']);
    }
    ?>
    <div class="row">
        <div class="col-lg-8">
            <div class="well">
                    <div class="form-group has-success ">
                        <label class="control-label " >Student Reg No</label>
                        <select class="form-control" id="sel_depart" name="stu_id">
                            <option value="">Select a student registation number</option>
                            <?php
                            foreach ($stu as $stid)
                            {   ?>
                                <option value="<?php echo $stid['id']; ?>"><?php echo $stid['stu_id']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Name</label>
                        <input type="text" class="form-control" id="stname">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Email</label>
                        <input type="text" class="form-control" id="stemail">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Department</label>
                        <input type="text" class="form-control" id="stdpt">
                    </div>


                     <span id="utility">Dowanload<a href="pdf.php?id=<?php echo $_GET['id']; ?>">PDF</a> </span>
                    <table class="table table-striped table-hover text-center" id="">
                        <thead>
                        <tr style="background-color:steelblue;color: white;font-weight: bold;font-size:20px; ">
                            <td>SL.No</td>
                            <td>Course Code</td>
                            <td>Course Name</td>
                            <td>GPA</td>
                        </tr>
                        </thead>
                        <tbody id="sel_all">

                        </tbody>

                    </table>







                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /.row -->
<?php include("footer.php"); ?>
