<?php
error_reporting(0);
include ('../vendor/autoload.php');
 use App\teacher\teacher;
 use App\database\database;

 $obj=new teacher();
 $tech=$obj->viewTeacher($_GET['id']);
?>


<?php include("header.php"); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-6">
            <h2 class="page-header">Edit Teacher</h2>

        </div>
        <div class="col-lg-6">
            <h2 class="page-header"><a href="view_teacher.php"> View Teacher</a></h2>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6">
            <div class="well">
                <?php
                //session_start();
                if(isset($_SESSION['Message'])){
                    echo $_SESSION['Message'];
                    unset ($_SESSION['Message']);
                }
                ?>
                <form action="../store/teacher_update.php" method="post">
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Name</label>
                        <input type="text" class="form-control" id="inputSuccess" name="t_name" value="<?php echo $tech['t_name']; ?>">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Address</label>
                        <textarea type="text" class="form-control" name="t_address" id="inputSuccess" cols="12" rows="5"><?php echo $tech['t_address']; ?></textarea>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Email</label>
                        <input type="email" class="form-control" name="t_email" id="inputSuccess" value="<?php echo $tech['t_email']; ?>">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Contaact No</label>
                        <input type="text" class="form-control" name="t_contact" id="inputSuccess" value="<?php echo $tech['t_contact']; ?>">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label">Designation</label>
                        <select class="form-control" name="t_designation">
                            <option value="">Select a designation</option>
                            <?php
                           // include ('config.php');
                            $db = database::getInstance();
                            $statement= $db->prepare("SELECT * FROM designations ORDER BY id ASC");
                            $statement->execute();
                            $result2= $statement->fetchAll(PDO::FETCH_ASSOC);
                            foreach($result2 as $row3)
                            {
                                if($row3['id']==$tech['t_designation'])
                                {
                                    ?><option value= "<?php echo $row3['id']; ?>" selected><?php echo $row3['designation']; ?></option><?php
                                }
                                else
                                {
                                    ?>
                                    <option value= "<?php echo $row3['id']; ?>"><?php echo $row3['designation']; ?></option>
                                    <?php
                                }
                            }

                            ?>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label">Department</label>
                        <select class="form-control" name="t_department">
                            <option value="">Select a depatment</option>
                            <?php
                            //include ('config.php');

                            $statement= $db->prepare("SELECT * FROM departments WHERE value=0 ORDER BY department ASC");
                            $statement->execute();
                            $result= $statement->fetchAll(PDO::FETCH_ASSOC);
                            foreach($result as $row4)
                            {
                                if($row4['id']==$tech['t_department'])
                                {
                                    ?><option value= "<?php echo $row4['id']; ?>" selected><?php echo $row4['department']; ?></option><?php
                                }
                                else
                                {
                                    ?>
                                    <option value= "<?php echo $row4['id']; ?>"><?php echo $row4['department']; ?></option>
                                    <?php
                                }
                            }

                            ?>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Credit to be taken</label>
                        <input type="text" class="form-control" name="t_credit" id="inputSuccess" value="<?php echo $tech['t_credit']; ?>">
                    </div>
                    <button type="submit" class="btn btn-lg btn-success">Update</button>
                    <input type="hidden" name="tech_id" value="<?php echo $_GET['id']; ?>">
                </form>




                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /.row -->
<?php include("footer.php"); ?>
