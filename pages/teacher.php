
<?php
error_reporting(0);
include("../vendor/autoload.php");

use App\department\department;
use App\teacher\teacher;

$object=new teacher();
$object->addTeacher($_POST)->storeTeacher();

$obj2=new department();
$row2=$obj2->getDepartment();

$des=new teacher();
$row=$des->getDesignation();

?>

<?php include("header.php"); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-6">
            <h2 class="page-header">Teacher Registation Form</h2>
        </div>
        <div class="col-lg-6">
            <h2 class="page-header"><a href="view_ajaxTeacher.php"> View All Teacher</a></h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6">
            <div class="well">
                <?php
                //session_start();
                if(isset($_SESSION['Message'])){
                    echo $_SESSION['Message'];
                    unset ($_SESSION['Message']);
                }
                ?>
                <form action="" method="post">
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Name</label>
                        <input type="text" class="form-control" id="inputSuccess" name="t_name">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Address</label>
                        <textarea type="text" class="form-control" name="t_address" id="inputSuccess" cols="12" rows="5"></textarea>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Email</label>
                        <input type="email" class="form-control" name="t_email" id="inputSuccess">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Contaact No</label>
                        <input type="text" class="form-control" name="t_contact" id="inputSuccess">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label">Designation</label>
                        <select class="form-control" name="t_designation">
                            <option value="">Select a designation</option>
                            <?php
                            foreach ($row as $value)
                            { ?>
                            <option value="<?php echo $value['id']; ?>"><?php echo $value['designation']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label">Department</label>
                        <select class="form-control" name="t_department">
                            <option value="">Select a depatment</option>
                            <?php
                            foreach ($row2 as $dpt)
                            {   ?>
                                <option value="<?php echo $dpt['id']; ?>"><?php echo $dpt['department']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Credit to be taken</label>
                        <input type="text" class="form-control" name="t_credit" id="inputSuccess">
                    </div>
                    <button type="submit" class="btn btn-lg btn-success">Save</button>

                </form>




                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /.row -->
<?php include("footer.php"); ?>
