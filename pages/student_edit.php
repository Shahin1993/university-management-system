<?php
error_reporting(0);
include('../vendor/autoload.php');
use App\department\department;
use App\student\student;

$row=new department();
$value=$row->getDepartment();

$getstu=new student();
$stu=$getstu->showStudent($_GET['id']);

?>
<?php include("header.php"); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-6">
            <h2 class="page-header">Edit Student information</h2>
        </div>
        <div class="col-lg-6">
            <h2 class="page-header"><a href="view_student.php">View Student</a></h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-6">
            <div class="well">
                <?php
                session_start();
                if(isset($_SESSION['Message'])){
                    echo $_SESSION['Message'];
                    unset ($_SESSION['Message']);
                }
                ?>

                <form action="../store/student_update.php" method="post">
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Name</label>
                        <input type="text" name="st_name" class="form-control" id="inputSuccess" value="<?php echo $stu['st_name']; ?>">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Email</label>
                        <input type="text" name="st_email" class="form-control" id="inputSuccess" value="<?php echo $stu['st_email']; ?>">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Contact No</label>
                        <input type="text" name="st_contact" class="form-control" id="inputSuccess" value="<?php echo $stu['st_contact']; ?>">
                    </div>
                    <div class="form-group  has-success">
                        <label  class="control-label" for="inputSuccess">Date</label>
                        <input  type="text" placeholder="click to show datepicker" name="date"  id="example1" value="<?php echo $stu['date']; ?>">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Address</label>
                        <textarea type="text" name="st_address" class="form-control" id="inputSuccess" cols="12" rows="5"><?php echo $stu['st_address']; ?></textarea>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" >Department</label>
                        <select class="form-control" name="st_department">
                            <option value="">Select</option>
                            <?php
                            //
                            foreach($value as $row4)
                            {
                                if($row4['d_code']==$stu['st_department'])
                                {
                                    ?><option value= "<?php echo $row4['d_code']; ?>" selected><?php echo $row4['department']; ?></option><?php
                                }
                                else
                                {
                                    ?>
                                    <option value= "<?php echo $row4['d_code']; ?>"><?php echo $row4['department']; ?></option>
                                    <?php
                                }
                            }

                            ?>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-lg btn-success">Update</button>
                    <input type="hidden" name="st_id" value="<?php echo $_GET['id']; ?>">

                </form>




                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-12 -->
    </div>
</div>
<footer id="footer-bottom" class="well text-center">

    <div class="copyrights ">Copyright &copy; LEBS CODING 2017 | <a href="#">lebscoding.com</a></div>
    <!-- End social_icons -->

</footer>

<!-- /.row -->
</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>
<script src="../vendor/bootstrap/js/datepicker.js"></script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#example1').datepicker({
            format: "dd/mm/yyyy"
        });

    });
</script>

</body>

</html>
