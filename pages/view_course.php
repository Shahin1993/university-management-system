<?php
include ('../vendor/autoload.php');
use App\course\course;
use App\database\database;

$obj=new course();
$row=$obj->viewAllcourse();
?>

    <?php include("header.php"); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-9">
                    <h1 class="page-header">View All Course</h1>
                </div>
                    <div class="col-lg-3">
                        <h1 class="page-header"><a href="course.php">Add Course</a> </h1>
                    </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <?php
                        //session_start();
                        if(isset($_SESSION['Message'])){
                            echo $_SESSION['Message'];
                            unset ($_SESSION['Message']);
                        }
                        ?>

                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover text-center">
                                    <thead>
                                        <tr style="background-color:steelblue;color: white;font-weight: bold;font-size:20px; ">
                                            <td>No</td>
                                            <td>C-Code</td>
                                            <td>Name</td>
                                            <td>Credit</td>
                                            <td>Department</td>
                                            <td>Semester</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=0;
                                    foreach ($row as $crs)

                                    {
                                        $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $crs['c_code']; ?></td>
                                            <td><?php echo $crs['c_name']; ?></td>
                                            <td><?php echo $crs['c_credit']; ?></td>
                                            <td>

                                                <?php

                                                $db = database::getInstance();
                                                $statement= $db->prepare("SELECT * FROM departments WHERE id=?");
                                                $statement->execute(array($crs['department_id']) );
                                                $result= $statement->fetchAll(PDO::FETCH_ASSOC);
                                                foreach ($result as $dpart)
                                                {
                                                    echo $dpart['department'];
                                                }
                                                ?>

                                            </td>
                                            <td>
                                                <?php


                                                $db = database::getInstance();
                                                $statement= $db->prepare("SELECT * FROM semesters WHERE id=?");
                                                $statement->execute(array($crs['semester_id']) );
                                                $result1= $statement->fetchAll(PDO::FETCH_ASSOC);
                                                foreach ($result1 as $sem)
                                                {
                                                    echo $sem['semester'];
                                                }
                                                ?>

                                            </td>

                                        </tr>

                                    <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->

                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        </div>
    <?php include("footer.php"); ?>



