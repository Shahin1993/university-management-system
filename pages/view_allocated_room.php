<?php
include ('../vendor/autoload.php');
use App\department\department;

$obj=new department();
$row=$obj->getDepartment();
?>

    <?php include("header.php"); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-6">
                    <h4 class="page-header">View Class Schedule And Room Allocation Information</h4>

                </div>
                <div class="col-lg-6">
                    <h4 class="page-header"><a href="allocate_classrooms.php"> Add Class Schedule And Room</a> </h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                    <div class="panel panel-default">
                        <?php
                        //session_start();
                        if(isset($_SESSION['Message'])){
                            echo $_SESSION['Message'];
                            unset ($_SESSION['Message']);
                        }
                        ?>

                        <div class="panel-body">
                            <div class="table-responsive">
                                <div class="col-lg-2"></div>
                                    <div class="col-lg-4">
                                    <div class="form-group has-success text-center">
                                        <label class="control-label"><h2>Select Department</h2></label>
                                        <select class="form-control text-center" name="department_id" id="deptId" >
                                            <option value="1">Select Department</option>
                                            <option value="">Show All Course</option>
                                            <?php
                                            foreach ($row as $dpt)
                                            { ?>
                                                <option value="<?php echo $dpt['id']; ?>"><?php echo $dpt['department']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    </div>
                                    <div class="col-lg-9">
                                    <h3>Course Information</h3>
                                    <table class="table table-striped table-hover text-center" id="">
                                        <thead>
                                        <tr style="background-color:steelblue;color: white;font-weight: bold;font-size:20px; ">
                                            <td>Course Code</td>
                                            <td>Course Name</td>
                                            <td>Class schedule info</td>


                                        </tr>
                                        </thead>
                                        <tbody id="showSchedule">

                                        </tbody>

                                    </table>

                            </div>

                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->

                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        </div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    $(document).ready(function () {
        $('#deptId').change(function () {
            var scheduleId = $(this).val();
            $.ajax({
                url:"loader.php",
                method:"POST",
                data:{scheduleId:scheduleId},
                success:function (data) {
                    $('#showSchedule').html(data);
                }
            });
        });
    });
</script>
    <?php include("footer.php"); ?>



