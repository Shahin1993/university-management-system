<?php
error_reporting(0);
include ('../vendor/autoload.php');
use App\course\course;
use App\semester\semester;
use App\department\department;


 $obj2=new department();
 $result=$obj2->getDepartment();

$obj=new course();
$row2=$obj->viewcourse($_GET['id']);

$sem=new semester();
$row=$sem->getSemester();
?>


<?php include("header.php"); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-6">
            <h2 class="page-header">Edit Course</h2>

        </div>
        <div class="col-lg-6">
            <h2 class="page-header"><a href="view_course.php"> View Courses</a></h2>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6">
            <div class="well">


                <form action="../store/course_update.php" method="post">
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Code</label>
                        <input type="text" class="form-control" id="inputSuccess" name="c_code" value="<?php echo $row2['c_code']; ?>">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Name</label>
                        <input type="text" class="form-control" id="inputSuccess" name="c_name" value="<?php echo $row2['c_name']; ?>">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Credit</label>
                        <input type="text" class="form-control" id="inputSuccess" name="c_credit" value="<?php echo $row2['c_credit']; ?>">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Description</label>
                        <textarea type="text" class="form-control" id="inputSuccess" name="c_description" cols="12" rows="5" ><?php echo $row2['c_description']; ?></textarea>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" >Department</label>
                        <select class="form-control" name="department_id">
                            <option>Select</option>
                            <?php
//
                            foreach($result as $row4)
                            {
                                if($row4['id']==$row2['department_id'])
                                {
                                    ?><option value= "<?php echo $row4['id']; ?>" selected><?php echo $row4['department']; ?></option><?php
                                }
                                else
                                {
                                    ?>
                                    <option value= "<?php echo $row4['id']; ?>"><?php echo $row4['department']; ?></option>
                                    <?php
                                }
                            }

                            ?>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label">Semester</label>
                        <select class="form-control" name="semester_id">
                            <option>Select</option>
                            <?php

                            foreach ($row as $value)
                            {
                                if($value['id']==$row2['semester_id'])
                                {
                                    ?><option value= "<?php echo $value['id']; ?>" selected><?php echo $value['semester']; ?></option><?php
                                }

                                 else { ?>
                                     <option value="<?php echo $value['id']; ?>"><?php echo $value['semester']; ?></option>
                                     <?php
                                 }
                            } ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-lg btn-success">Update</button>
                    <input type="hidden" name="c_id" value="<?php echo $_GET['id']; ?>">

                </form>




                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /.row -->
<?php include("footer.php"); ?>
