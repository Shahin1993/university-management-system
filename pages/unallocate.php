

<?php
include ('../vendor/autoload.php');
//use App\Session\Session;
use App\allocateClass\allocateClass;
$uncl=new allocateClass();

?>

<?php include "header.php"?>

<div id="page-wrapper">


    <div class="row ">
        <div class="col-lg-12">
            <h2 class="page-header">Unallocate All Rooms</h2>
        </div>

        <!-- /.col-lg-12 -->
    </div>

    <?php
    session_start();
    if(isset($_SESSION['Message'])){
        echo $_SESSION['Message'];
        unset ($_SESSION['Message']);
    }
    ?>
    <!-- Main content -->
    <div class="row">
    <section class="content">
        <div class="container">


            <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Unallocate Courses</button>

            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><b>Unallocate All Classrooms !!! </b></h4>
                        </div>
                        <div class="modal-body">
                            <p style="color: #ac2925">Do you really sure you want to unallocate all classrooms!!!.</p>
                        </div>
                        <?php
                        if (isset($_GET["action"]) && $_GET["action"] == "unallocate"){
                           $uncl-> unAllocate();
                        }
                        ?>
                        <div class="modal-footer">
                            <a href="?action=unallocate" type="button" class="btn
                            btn-default">Yes</a>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
</div>
<?php include "footer.php"?>
