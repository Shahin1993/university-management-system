<?php
error_reporting();
include("../vendor/autoload.php");

use App\department\department;

$obj2=new department();
$row2=$obj2->getDepartment();

?>
<?php include("header.php"); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-8">
            <h2 class="page-header">Course Assign To Teacher</h2>
        </div>
        <div class="col-lg-4">
            <h2 class="page-header"><a href="view_course_statics.php"> View Course statics</a></h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script>

        $(document).ready(function(){

            $("#sel_depart").change(function(){
                var deptid = $(this).val();

                $.ajax({
                    url: '../store/get_course.php',
                    type: 'post',
                    data: {depart:deptid},
                    dataType: 'json',
                    success:function(response){

                        var len = response['teacher'].length;

                        $("#sel_teacher").empty();
                        $("#sel_teacher").append("<option value=''>"+'Select Teacher Name'+"</option>");
                        for( var i = 0; i<len; i++){
                            var id = response['teacher'][i]['t_id'];
                            var name = response['teacher'][i]['t_name'];
                            $("#sel_teacher").append("<option value='"+id+"'>"+name+"</option>");

                        }

                        var clen = response['course'].length;

                        $("#sel_coursecode").empty();
                        $("#sel_coursecode").append("<option value=''>"+'Select Course Code'+"</option>");
                        for( var i = 0; i<clen; i++){
                            var cid = response['course'][i]['id'];
                            var code = response['course'][i]['c_code'];
                            $("#sel_coursecode").append("<option value='"+cid+"'>"+code+"</option>");

                        }

                    }
                });


            });

        });

    </script>
    <script>

        $(document).ready(function(){

            $("#sel_teacher").change(function () {
                var teachid = $(this).val();

                $.ajax({
                    url: '../store/get_teacherscredit.php',
                    type: 'post',
                    data: {teacher: teachid},
                    dataType: 'json',
                    success: function (response) {

                        var crd = response['teacher']['t_credit'];
                        var ac=response['a_c']['assign_credit'];

                        $("#takencredit").val(crd)
                        $("#rc").val(crd-ac)


                    }
                });
            });

         $("#sel_coursecode").change(function () {
            var courseid = $(this).val();

            $.ajax({
                url: '../store/ajax_course.php',
                type: 'post',
                data: {course: courseid},
                dataType: 'json',
                success: function (response) {

                    var crn = response['c_name'];
                    var cr=response['c_credit'];
                    $("#coursename").val(crn)
                    $("#credit").val(cr)


                }
            });
        });

        });

    </script>
    <div class="row">
        <div class="col-lg-6">
            <div class="well">
                <?php
                //session_start();
                if(isset($_SESSION['Message'])){
                    echo $_SESSION['Message'];
                    unset ($_SESSION['Message']);
                }
                ?>
<!--<a href="../store/get_teacherscredit.php">Get course</a>--->
                <form action="../store/get_courseAssign.php" method="post">
                    <div class="form-group has-success">
                        <label class="control-label" >Department</label>
                        <select class="form-control" name="department_id" id="sel_depart">

                            <option value="">Select a Department</option>
                            <?php
                            foreach ($row2 as $dpt)
                            {   ?>
                                <option value="<?php echo $dpt['id']; ?>"><?php echo $dpt['department']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" >Teacher</label>
                        <select class="form-control" id="sel_teacher" name="teacher_id">
                            <option value="">Select a Teacher Name</option>

                        </select>
                    </div>
                    <div class="form-group has-success" >
                        <label class="control-label" >Cedit to be taken</label>
                        <input type="text" name="st_contact" class="form-control" id="takencredit">
                    </div>

                   <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Remaining Credit</label>
                       <input type="text" name="st_contact" class="form-control" id="rc">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label">Course Code</label>
                        <select class="form-control" name="course_code" id="sel_coursecode">
                            <option value="">Select a course code</option>

                        </select>
                    </div >
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Course Name</label>
                        <input type="text" name="st_contact" class="form-control" id="coursename">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Course Credit</label>
                        <input type="text" name="c_credit" class="form-control" id="credit">

                    </div>
                    <button type="submit" class="btn btn-lg btn-success">Assign</button>

                </form>




                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-12 -->

    </div>
</div>



<!-- /.row -->
<?php include("footer.php"); ?>
