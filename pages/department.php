<?php
error_reporting(0);
include("../vendor/autoload.php");
use App\department\department;

$obj=new department();
$obj->addDepartment($_POST)->storeDepartment();

?>
<?php include("header.php"); ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Save Department
                    </h1>

                </div>
                <h3><a href="view_department.php">View All Department</a> </h3>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-6">
                    <div class="well">
                        <?php
                        //session_start();
                        if(isset($_SESSION['Message'])){
                            echo $_SESSION['Message'];
                            unset ($_SESSION['Message']);
                        }
                        ?>

                                <form action="" method="post">

                                    <div class="form-group has-success">
                                        <label class="control-label" for="inputSuccess">Code</label>
                                        <input type="text" name="d_code" class="form-control" id="inputSuccess" maxlength="7">
                                    </div>
                                    <div class="form-group has-success">
                                        <label class="control-label" for="inputSuccess">Department Name</label>
                                        <input type="text" name="department_n" class="form-control" id="inputSuccess">
                                    </div>
                                    <button type="submit" class="btn btn-lg btn-success">Save</button>

                                </form>




                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

                <!-- /.col-lg-12 -->
            </div>
        </div>
            <!-- /.row -->
       <?php include("footer.php"); ?>
