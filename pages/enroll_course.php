<?php
error_reporting(0);
include("../vendor/autoload.php");
use App\student\student;

$obj=new student();
$stu=$obj->showStudent_id();

?>


<?php include("header.php"); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Enroll in a course</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script>

        $(document).ready(function(){

            $("#sel_depart").change(function(){
                var deptid = $(this).val();

                $.ajax({
                    url: '../store/ajax_studentEnroll.php',
                    type: 'post',
                    data: {depart:deptid},
                    dataType: 'json',
                    success:function(response){


                        var crn = response['student']['st_name'];
                        var cr=response['student']['st_email'];
                        var dpt=response['student']['department'];
                        $("#stname").val(crn)
                        $("#stemail").val(cr)
                        $("#stdpt").val(dpt)

                        var slen = response['course'].length;

                        $("#sel_all").empty();
                        $("#sel_all").append ("<option value=''>"+'Select Course'+"</option>");
                        for (var i = 0; i < slen; i++) {


                            var cd = response['course'][i]['id'];
                            var cn = response['course'][i]['c_name'];

                            $("#sel_all").append ("<option value='"+cd+"'>"+cn+"</option>");

                        }
                    }

                });


            });

        });


    </script>
    <div class="row">
        <div class="col-lg-6">
            <div class="well">
                <?php
                //session_start();
                if(isset($_SESSION['Message'])){
                    echo $_SESSION['Message'];
                    unset ($_SESSION['Message']);
                }
                ?>

                <form action="../store/get_enrollCourseStudent.php" method="post">
                    <div class="form-group has-success">
                        <label class="control-label" >Student Reg No</label>
                        <select class="form-control" id="sel_depart" name="stu_id">
                            <option value="">Select a student registation number</option>
                            <?php
                            foreach ($stu as $stid)
                            {   ?>
                                <option value="<?php echo $stid['id']; ?>"><?php echo $stid['stu_id']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Name</label>
                        <input type="text" class="form-control" id="stname">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Email</label>
                        <input type="text" class="form-control" id="stemail">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Department</label>
                        <input type="text" class="form-control" id="stdpt">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" >Select Course</label>
                        <select class="form-control" name="course_id" id="sel_all">
                            <option value="">Select a course</option>

                        </select>
                    </div>
                    <div class="form-group  has-success">
                        <label  class="control-label" for="inputSuccess">Date</label>
                        <input  type="text" placeholder="click to show datepicker" name="st_date"  id="example1">
                    </div>
                    <button type="submit" class="btn btn-lg btn-success">Enroll</button>

                </form>




                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /.row -->

<footer id="footer-bottom" class="well text-center">

    <div class="copyrights ">Copyright &copy; LEBS CODING 2017 | <a href="#">lebscoding.com</a></div>
    <!-- End social_icons -->

</footer>

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>
<script src="../vendor/bootstrap/js/datepicker.js"></script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#example1').datepicker({
            format: "dd/mm/yyyy"
        });

    });
</script>

</body>

</html>
