<?php
error_reporting(0);
include ('../vendor/autoload.php');
use App\department\department;

$obj=new department();
$row=$obj->getDepartment();
?>

    <?php include("header.php"); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-9">
                    <h1 class="page-header">View Course Statics</h1>

                </div>
                <div class="col-lg-3">
                    <h1 class="page-header"><a href="courseAssign.php">Course Assign </a> </h1>

                </div>
                <!-- /.col-lg-12 -->
            </div>


            <?php
            //session_start();
            if(isset($_SESSION['Message'])){
                echo $_SESSION['Message'];
                unset ($_SESSION['Message']);
            }
            ?>
            <div class="row">
                <div class="">
                    <div class="panel panel-default">


                        <div class="panel-body">
                            <div class="table-responsive">
                                <div class="col-lg-4"></div>
                                <div class=" text-center form-group has-success col-lg-4">
                                    <label class="control-label "><h2>Select Department</h2></label>
                                    <select class="form-control text-center" name="department_id" id="semesters" >
                                        <option value="1">Select Department</option>
                                        <option value="">Show All Course</option>
                                        <?php
                                        foreach ($row as $dpt)
                                        { ?>
                                            <option value="<?php echo $dpt['id']; ?>"><?php echo $dpt['department']; ?></option>
                                        <?php } ?>
                                    </select>

                                </div>
                                <div class="col-lg-4"></div>
                                <div class="col-lg-12"
                                <form>
                                <h3>Course Information</h3>

                                <table class="table table-striped table-hover text-center">
                                   <thead>
                                        <tr style="background-color:steelblue;color: white;font-weight: bold;font-size:20px; ">
                                            <td>Sl.no</td>
                                            <td>Course Code</td>
                                            <td>course Name</td>
                                            <td>Semester</td>
                                            <td>Assigned To</td>
                                        </tr>
                                   </thead>
                                    <tbody id="show_courses">

                                    </tbody>

                                </table>
                                </form>
                            </div>

                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->

                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        </div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>



<script>
    $(document).ready(function () {
        $('#semesters').change(function () {
            var semester_id = $(this).val();
            $.ajax({
                url:"loader.php",
                method:"post",
                data:{semester_id:semester_id},
                success:function (data) {
                    $('#show_courses').html(data);
                }
            });
        });
    });
</script>

    <?php include("footer.php"); ?>



