<?php
 include ('../vendor/autoload.php');
 USE App\database\database;

 ?>

<?php
$output='';
if (isset($_POST["semester_id"])){
   $depart_id=$_POST["semester_id"];
 if ($_POST["semester_id"] != '') {

     $sql = "SELECT * FROM courses LEFT JOIN semesters on courses.semester_id=semesters.id LEFT JOIN course_teacher on courses.id = course_teacher.course_code LEFT JOIN teachers on 
course_teacher.teacher_id = teachers.t_id  WHERE courses.department_id=$depart_id AND courses.status=1";
 }
 else{
     $sql = "SELECT * FROM courses LEFT JOIN semesters on courses.semester_id=semesters.id LEFT JOIN course_teacher on courses.id = course_teacher.course_code LEFT JOIN teachers on 
course_teacher.teacher_id = teachers.t_id WHERE courses.status=1 ORDER BY courses.id DESC";
 }
    $db = database::getInstance();
    $stmt = $db->Prepare($sql);
    $stmt->execute();
    $data =  $stmt->fetchAll();
    $output .='<table id="table_info" class="table table-striped able-hover text-center">';

    $i = 0;
    foreach ($data as $val){
        $i++;
        $output .= '<tr>';
        $output .= '<td>'. $i .'</td>';
        $output .= '<td>' .$val["c_code"].'</td>';
        $output .= '<td>' .$val["c_name"].'</td>';
        $output .= '<td>' .$val["semester"].'</td>';
        if ($val["t_name"] == '' || $val['stat']==1){
            $val["t_name"] = "Not Assigned Yet";
        }
        $output .= '<td>' .$val["t_name"].'</td>';
        $output .= '</tr>';
    }

    echo $output;
}


if (isset($_POST["scheduleId"])){

    if ($_POST["scheduleId"] != ''){
        $db = database::getInstance();
        $sql = 'SELECT * FROM courses WHERE department_id = "'.$_POST["scheduleId"].'"AND status=1 ';
    }else{
        $db = database::getInstance();
        $sql = 'SELECT * FROM courses WHERE status=1';
    }
    //$db = database::getInstance();
    $stmt = $db->Prepare($sql);
    $stmt->execute();
    $courseDetail =  $stmt->fetchAll();
    foreach ($courseDetail as $data){
        $output .='<tr>';
        $output .= '<td>' . $data["c_code"] . '</td>';
        $output .= '<td>' .  $data["c_name"] . '</td>';
        $output .= '<td>';

        $db = database::getInstance();
        $sql = 'SELECT rooms.room_no, days.day, course_class_allocate.id, course_class_allocate.start_time, course_class_allocate.end_time FROM course_class_allocate LEFT JOIN rooms ON course_class_allocate.room_id=rooms.id LEFT JOIN days ON course_class_allocate.day_id=days.id WHERE course_class_allocate.course_id =  "'.$data["id"] .'" AND value=0';
        $stmt =$db->Prepare($sql);
        $stmt->execute();
        $totalRows =  $stmt->rowCount();
        $schedules =  $stmt->fetchAll();
        if ($totalRows < 1){
            $output .='Not Scheduled Yet';
        }else {
            foreach ($schedules as $schedule) {
                $output .= "Room No : " . $schedule["room_no"] . ", " . $schedule["day"] . " , " .
                    date("g:i a", strtotime($schedule["start_time"])) . " - " . date("g:i a", strtotime($schedule["end_time"])) . "<br>";
            }
        }

        $output .= '</td>';
        $output .='</tr>';
    }
    echo $output;
}

if (isset($_POST["grade"])){
    $query = "SELECT courses.c_name, courses.c_code, grades.grade,results.id FROM results INNER JOIN courses 
ON results.course_id=courses.id INNER JOIN grades ON results.grade_id=grades.id WHERE results.student_reg_no = '" .$_POST["grade"]."'";

    $db = database::getInstance();
    $stmt = $db->Prepare($query);
    $stmt->execute();
    $data =  $stmt->fetchall();
    foreach ($data as $results) {
        $output .='<tr>';
        $output .= '<td>' . $results['c_code'] . '</td>';
        $output .= '<td>' . $results['c_name'] . '</td>';
        $output .= '<td>' . $results['grade'] . '</td>';
        $output .='</tr>';
    }

    echo $output;
}
if(isset($_POST['studepart'])){
    if($_POST['studepart'] !=''){
        $db = database::getInstance();
        $query ='SELECT * FROM students WHERE  st_department="'.$_POST['studepart'].'" AND  status=1';
    }
    else{
        $db = database::getInstance();
        $query ='SELECT * FROM students  WHERE students.status=1';
    }
    $stmt=$db->prepare($query);
    $stmt->execute();
    $student=$stmt->fetchAll();
    $i=0;
    foreach ($student as $stu){
        $i++;
        $output .= '<tr>';
        $output .= '<td>'. $i .'</td>';
        $output .= '<td>' .$stu["stu_id"].'</td>';
        $output .= '<td>' .$stu["st_name"].'</td>';
        $output .= '<td>' .$stu["st_email"].'</td>';
        $output .= '<td>' .$stu["st_contact"].'</td>';
        $output .= '<td>' .$stu["date"].'</td>';
        $output .='<td>';
        $output .= '<a class="btn btn-default" href="student_edit.php?id='.$stu["id"].'">Edit</a>';
        $output .= '</td>';
        $output .='<td>';
        $output .= '<a onclick="return confirm(\'Are u sure want to delete this  ?\')" class="btn btn-danger" href= "softDeleteStudent.php?id='.$stu["id"].'">Delete</a>';
        $output .= '</td>';
        $output .= '<tr>';

    }
    echo $output;

}
if(isset($_POST["depart"])){
    $depart=$_POST["depart"];
    if($_POST["depart"] !=''){
        $db = database::getInstance();
        $sql1="SELECT * FROM courses LEFT JOIN semesters on courses.semester_id=semesters.id WHERE department_id =$depart AND courses.status=1";
    }
    else{
        $db = database::getInstance();
        $sql1 = "SELECT * FROM courses LEFT JOIN semesters on courses.semester_id=semesters.id WHERE courses.status=1";
    }
    $stmt2=$db->prepare($sql1);
    $stmt2->execute();
    $course=$stmt2->fetchAll();

    $i=0;
    foreach ($course as $crs){
        $i++;
        $output .= '<tr>';
        $output .= '<td>'. $i .'</td>';
        $output .= '<td>' .$crs["c_code"].'</td>';
        $output .= '<td>' .$crs["c_name"].'</td>';
        $output .= '<td>' .$crs["c_credit"].'</td>';
        $output .= '<td>' .$crs["semester"].'</td>';
        $output .='<td>';
        $output .= '<a class="btn btn-success" href="edit_course.php?id='.$crs["id"].'">Edit</a>';
        $output .= '</td>';
        $output .='<td>';
        $output .= '<a onclick="return confirm(\'Are u sure want to delete this  ?\')" class="btn btn-danger" href= "softdeleteCourse.php?id='.$crs["id"].'">Delete</a>';
        $output .= '</td>';
        $output .= '<tr>';

    }
    echo $output;
}


if(isset($_POST["tchdepat"])){
    $depart=$_POST["tchdepat"];
    if($_POST["tchdepat"] !=''){
        $db = database::getInstance();
        $sql1="SELECT * FROM teachers LEFT JOIN designations on teachers.t_designation=designations.id LEFT JOIN departments on teachers.t_department=departments.id WHERE t_department =$depart AND teachers.status=0";
    }
    else{
        $db = database::getInstance();
        $sql1 = "SELECT * FROM teachers LEFT JOIN designations on teachers.t_designation=designations.id LEFT JOIN departments on teachers.t_department=departments.id WHERE teachers.status=0";
    }
    $stmt2=$db->prepare($sql1);
    $stmt2->execute();
    $teach=$stmt2->fetchAll();

    $i=0;
    foreach ($teach as $tch){
        $i++;
        $output .= '<tr>';
        $output .= '<td>'. $i .'</td>';
        $output .= '<td>' .$tch["t_name"].'</td>';
        $output .= '<td>' .$tch["t_email"].'</td>';
        $output .= '<td>' .$tch["t_contact"].'</td>';
        $output .= '<td>' .$tch["designation"].'</td>';
        $output .= '<td>' .$tch["department"].'</td>';
        $output .= '<td>' .$tch["t_credit"].'</td>';

        $output .='<td>';
        $output .= '<a class="btn btn-success" href="teacher_edit.php?id='.$tch["t_id"].'">Edit</a>';
        $output .= '</td>';
        $output .='<td>';
        $output .= '<a onclick="return confirm(\'Are u sure want to delete this  ?\')" class="btn btn-danger" href= "softDeleteTeacher.php?id='.$tch["t_id"].'">Delete</a>';
        $output .= '</td>';
        $output .= '<tr>';

    }
    echo $output;
}


?>
