<?php
include('../vendor/autoload.php');
use App\department\department;
use App\room_day_grade\rdg;

$depart=new department();
$row2=$depart->getDepartment();

$row1=new rdg();
$room=$row1->getRoom();

$week=$row1->getDay();

?>


<?php include("header.php"); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-6">
            <h2 class="page-header">Allocate Classrooms</h2>
        </div>
        <div class="col-lg-6">
            <h2 class="page-header"><a href="view_allocated_room.php"> View Allocated Classrooms</a></h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script>

        $(document).ready(function(){

            $("#sel_depart").change(function(){
                var deptid = $(this).val();

                $.ajax({
                    url: '../store/get_course.php',
                    type: 'post',
                    data: {depart:deptid},
                    dataType: 'json',
                    success:function(response){


                        var clen = response['course'].length;

                        $("#sel_coursecode").empty();
                        $("#sel_coursecode").append("<option value=''>"+'Select Course'+"</option>");
                        for( var i = 0; i<clen; i++){
                            var cid = response['course'][i]['id'];
                            var code = response['course'][i]['c_name'];
                            $("#sel_coursecode").append("<option value='"+cid+"'>"+code+"</option>");

                        }

                    }
                });


            });

        });


    </script>


    <?php

//    if ($_SERVER["REQUEST_METHOD"] == "POST") {
//
//
//        if (!empty($this->course_id)) {
//            // $dptId = $this->department_id;
//            $courseId = $this->course_id;
//            $dayId = $this->day_id;
//
//            $db = database::getInstance();
//            $sql = "SELECT * FROM course_class_allocate WHERE course_id = '$courseId' AND day_id = '$dayId' ";
//            $stmt = $db->Prepare($sql);
//            $stmt->execute();
//            $courseFound = $stmt->fetchAll();
//            foreach ($courseFound as $course) {
//                if ($course["course_id"] = $courseId) {
//
//                    $_SESSION['Message'] = "This course already  scheduled today! Please select another course.";
//                    header("location: ../pages/allocate_classrooms.php");
//                }
//            }
//
//        }
//    }

    ?>


    <div class="row">
        <div class="col-lg-6">
            <div class="well">

                <?php
                //session_start();
                if(isset($_SESSION['Message'])){
                    echo $_SESSION['Message'];
                    unset ($_SESSION['Message']);
                }
                ?>
                <form action="../store/get_AllocateClass.php" method="post">
                    <div class="form-group has-success">
                        <label class="control-label" >Department</label>
                        <select class="form-control" id="sel_depart" name="department_id">
                            <option value="">Select department</option>
                            <?php
                            foreach ($row2 as $dpt)

                            { ?>
                                <option value="<?php echo $dpt['id']; ?>"><?php echo $dpt['department']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" >Course</label>
                        <select class="form-control" id="sel_coursecode" name="course_id">
                           <option value="">select course</option>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label">Room</label>
                        <select class="form-control" name="room_id">
                            <option value="">Select room</option>
                            <?php
                            foreach ($room as $value)

                            { ?>
                            <option value="<?php echo $value['id']; ?>"><?php echo $value['room_no']; ?></option>
                             <?php } ?>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" >Day</label>
                        <select class="form-control" name="day_id">
                            <option value="">Select day</option>
                            <?php
                            foreach ($week as $day)

                            { ?>
                                <option value="<?php echo $day['id']; ?>"><?php echo $day['day']; ?></option>
                            <?php } ?>
                        </select>
                    </div>


                        <div class="input-group bootstrap-timepicker timepicker has-success">
                            <label class="control-label" for="inputSuccess">From</label>
                            <input id="timepicker1" type="text" class="form-control input-small" name="start_time"  placeholder="clike to time setap">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                        </div>
                    <div class="input-group bootstrap-timepicker timepicker has-success">
                        <label class="control-label" for="inputSuccess">To</label>
                        <input id="timepicker2" type="text" class="form-control input-small" name="end_time"  placeholder="clike to time setap">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                    </div>
                        <br>

                    <button type="submit" class="btn btn-lg btn-success">Allocat</button>

                </form>




                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /.row -->

<footer id="footer-bottom" class="well text-center">

    <div class="copyrights ">Copyright &copy; LEBS CODING 2017 | <a href="#">lebscoding.com</a></div>
    <!-- End social_icons -->

</footer>
</div>

<!-- /#wrapper -->



<!-- jQuery -->
<script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<script src="../vendor/bootstrap/js/bootstrap-timepicker.min.js"> </script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>

<script type="text/javascript">
    $('#timepicker1').timepicker();
</script>
<script type="text/javascript">
    $('#timepicker2').timepicker();
</script>


</body>

</html>