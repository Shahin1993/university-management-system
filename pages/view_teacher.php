<?php
include ('../vendor/autoload.php');
use App\teacher\teacher;
use App\database\database;


$obj=new teacher();
$row=$obj->viewAllteacher();
?>

    <?php include("header.php"); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View All Teacher</h1>
                    <h3><a href="teacher.php">Add Teacher</a> </h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>

<!--            --><?php
////            SELECT
////            *,
////  concat(YEAR(NOW()),'-',LPAD(id,5,'0')) as stud_id
////FROM students
////
//
//             $db = new PDO('mysql:host=localhost;dbname=lebs', 'root', '');
//            $query =" SELECT
//            *,
//  concat(YEAR(NOW()),'-',LPAD(id,5,'0')) as stud_id
//FROM teachers";
//            $stmt = $db->prepare($query);
//            $stmt->execute();
//            $data = $stmt->fetchAll();
////            echo "<pre>";
////            print_r($data);
////            echo "</pre";$i=o
//
//        foreach ($data as $stu){
//
//        }
//            echo $stu['stud_id'] ;
//
//            ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <?php
                        //session_start();
                        if(isset($_SESSION['Message'])){
                            echo $_SESSION['Message'];
                            unset ($_SESSION['Message']);
                        }
                        ?>

                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover text-center">
                                    <thead>
                                        <tr style="background-color:steelblue;color: white;font-weight: bold;font-size:20px; ">
                                            <td>No</td>
                                            <td>Name</td>
                                            <td>Email</td>
                                            <td>Contact NO</td>
                                            <td>Designation</td>
                                            <td>Department</td>
                                            <td>Take Credit</td>
                                            <td>Action</td>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=0;
                                    foreach ($row as $tch)

                                    {
                                        $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $tch['t_name']; ?></td>
                                            <td><?php echo $tch['t_email']; ?></td>
                                            <td><?php echo $tch['t_contact']; ?></td>
                                            <td>

                                                <?php
                                               // include ('config.php');

                                               // $db = new PDO('mysql:host=localhost;dbname=lebs', 'root', '');
                                                $db = database::getInstance();
                                                $statement= $db->prepare("SELECT * FROM designations WHERE id=?");
                                                $statement->execute(array($tch['t_designation']) );
                                                $result= $statement->fetchAll(PDO::FETCH_ASSOC);
                                                foreach ($result as $man1)
                                                {
                                                    echo $man1['designation'];
                                                }
                                                ?>

                                            </td>
                                            <td>

                                                <?php
                                               // include ('config.php');

                                                //$db = new PDO('mysql:host=localhost;dbname=lebs', 'root', '');
                                                $statement= $db->prepare("SELECT * FROM departments WHERE id=?");
                                                $statement->execute(array($tch['t_department']) );
                                                $result= $statement->fetchAll(PDO::FETCH_ASSOC);
                                                foreach ($result as $man)
                                                {
                                                    echo $man['department'];
                                                }
                                                ?>
                                                 </td>
                                            <td><?php echo $tch['t_credit']; ?></td>
                                            <td>
                                            <a href="teacher_edit.php?id=<?php echo $tch['id']; ?>">Edit</a>|
                                                &nbsp;<a onclick= 'return confirmDelete();' href= "softDeleteTeacher.php?id=<?php echo $tch['id']; ?>" >
                                                    Delete</a></td>
                                            </td>

                                        </tr>
                                    <?php
                                    } ?>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->

                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        </div>
    <?php include("footer.php"); ?>



