<?php

include ('../vendor/mpdf/mpdf/mpdf.php');
include_once ('../vendor/autoload.php');
use App\studentResult\studentResult;


$obj=new studentResult();
$allData=$obj->viewAllResult();
$trs="";
$serial=0;

foreach ($allData as $data):
    $serial++;
    $trs.="<tr>";
    $trs.="<td>;".$serial."</td>";
    $trs.="<td>;".$data['c_code']."</td>";
    $trs.="<td>;".$data['c_name']."</td>";
    $trs.="<td>;".$data['grade_id']."</td>";

    $trs.="</tr>";
    endforeach;

$html = <<<EOD
<!DOCTYPE html>
<html>
<head>
   <title>List Of Result</title>
</head>
 <body>
    <h1>List Of Result</h>
    <table border="1">
       <thead>
       <tr>
           <th>SL.</th>
           <th>Course code</th>
           <th>Course Name</th>
           <th>GPA</th>
       </tr>
     </thead>
     
     <tbody>
        $trs;
     </tbody>
           
    
    </table>
   
 
 </dody>


</html>


EOD;


$mpdf =new mPDF();

$mpdf->WriteHTML($html);
$mpdf->Output();

exit;
?>
