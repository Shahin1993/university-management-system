
<?php
error_reporting(0);
include("../vendor/autoload.php");
use App\semester\semester;
use App\department\department;
use App\course\course;

$obj = new course();
$row = $obj->addCourse($_POST)->storeCourse();


$obj=new semester();
$row=$obj->getSemester();

$obj2=new department();
$row2=$obj2->getDepartment();

?>

<?php include("header.php"); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-6">
            <h2 class="page-header">Save Course</h2>
        </div>
        <div class="col-lg-6">
            <h2 class="page-header"><a href=" view_ajaxCourse.php">Course view</a></h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6">
            <div class="well">

                <?php
                //session_start();
                if(isset($_SESSION['Message'])){
                    echo $_SESSION['Message'];
                    unset ($_SESSION['Message']);
                }
                ?>
                <form action="" method="post">
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Code</label>
                        <input type="text" class="form-control" id="inputSuccess" name="c_code">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Name</label>
                        <input type="text" class="form-control" id="inputSuccess" name="c_name">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Credit</label>
                        <input type="text" class="form-control" id="inputSuccess" name="c_credit">
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Description</label>
                        <textarea type="text" class="form-control" id="inputSuccess" name="c_description" cols="12" rows="5"></textarea>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" >Department</label>
                        <select class="form-control" name="department_id">
                            <option value="">Select</option>
                            <?php
                            foreach ($row2 as $dpt)
                            {   ?>
                            <option value="<?php echo $dpt['id']; ?>"><?php echo $dpt['department']; ?></option>
                             <?php } ?>
                        </select>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label">Semester</label>
                        <select class="form-control" name="semester_id">
                            <option value="">Select</option>
                            <?php

                            foreach ($row as $value)

                            {  ?>
                            <option value="<?php echo $value['id']; ?>"><?php echo $value['semester']; ?></option>
                           <?php } ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-lg btn-success" name="form">Save</button>

                </form>




                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /.row -->
<?php include("footer.php"); ?>
