<?php
include ('../vendor/autoload.php');
use App\department\department;

$obj=new department();
$row=$obj->getDepartment();
?>

<?php include("header.php"); ?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-9">
            <h1 class="page-header">View All Course</h1>
        </div>
        <div class="col-lg-3">
            <h1 class="page-header"><a href="course.php">Add Course</a> </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php
    //session_start();
    if(isset($_SESSION['Message'])){
        echo $_SESSION['Message'];
        unset ($_SESSION['Message']);
    }
    ?>

    <div class="row text-center">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <label class="control-label"><h2>Select Department</h2></label>
            <select class="form-control text-center" name="department_id" id="deptId" >
                <option value="1">Select Department</option>
                <option value="">Show All Course</option>
                <?php
                foreach ($row as $dpt)
                { ?>
                    <option value="<?php echo $dpt['id']; ?>"><?php echo $dpt['department']; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-lg-4"></div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">


                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover text-center">
                            <thead>
                            <tr style="background-color:steelblue;color: white;font-weight: bold;font-size:20px; ">
                                <td>No</td>
                                <td>C-Code</td>
                                <td>Name</td>
                                <td>Credit</td>
                                <td>Semester</td>
                                <td>Edit</td>
                                <td>Delete</td>

                            </tr>
                            </thead>
                            <tbody id="showCourse">

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->

        <!-- /.col-lg-6 -->
    </div>
    <!-- /.row -->
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    $(document).ready(function () {
        $('#deptId').change(function () {
            var depart = $(this).val();
            $.ajax({
                url:"loader.php",
                method:"POST",
                data:{depart:depart},
                success:function (data) {
                    $('#showCourse').html(data);
                }
            });
        });
    });
</script>
<?php include("footer.php"); ?>



