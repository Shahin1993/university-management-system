<?php
include ('../vendor/autoload.php');
use App\department\department;

$obj=new department();
$row=$obj->getDepartment();
?>

    <?php include("header.php"); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-9">
                    <h1 class="page-header">View All Student</h1>
                </div>
                <div class="col-lg-3">
                    <h1 class="page-header"><a href="student_register.php">Add Student</a> </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <?php
            session_start();
            if(isset($_SESSION['Message'])){
                echo $_SESSION['Message'];
                unset ($_SESSION['Message']);
            }
            ?>
  <div class="row text-center">
      <div class="col-lg-4"></div>
      <div class="col-lg-4">
          <label class="control-label"><h2>Select Department</h2></label>
          <select class="form-control text-center" name="department_id" id="deptId" >
              <option value="1">Select Department</option>
              <option value="">Show All Student</option>
              <?php
              foreach ($row as $dpt)
              { ?>
                  <option value="<?php echo $dpt['d_code']; ?>"><?php echo $dpt['department']; ?></option>
              <?php } ?>
          </select>
      </div>
      <div class="col-lg-4"></div>
  </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover text-center">
                                    <thead>
                                        <tr style="background-color:steelblue;color: white;font-weight: bold;font-size:20px; ">
                                            <td>No</td>
                                            <td>Student ID</td>
                                            <td>Name</td>
                                            <td>Email</td>
                                            <td>Contact NO</td>
                                            <td>Date</td>
                                            <td>Edit</td>
                                            <td>Delete</td>

                                        </tr>
                                    </thead>
                                    <tbody id="showStudent" >


                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->

                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    $(document).ready(function () {
        $('#deptId').change(function () {
            var studepart = $(this).val();
            $.ajax({
                url:"loader.php",
                method:"POST",
                data:{studepart:studepart},
                success:function (data) {
                    $('#showStudent').html(data);
                }
            });
        });
    });
</script>
    <?php include("footer.php"); ?>



