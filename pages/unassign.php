
<?php

include ('../vendor/autoload.php');
use App\courseAssign\courseAssign;
$unasn=new courseAssign();

?>

<?php include "header.php"?>

<div id="page-wrapper">
    <!-- Main content -->

    <div class="row ">
        <div class="col-lg-12">
            <h2 class="page-header">Unassign All Courses</h2>
        </div>

        <!-- /.col-lg-12 -->
    </div>

    <?php
    session_start();
    if(isset($_SESSION['Message'])){
        echo $_SESSION['Message'];
        unset ($_SESSION['Message']);
    }
    ?>
    <div class="row">
    <section class="content">
        <div class="container">



            <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Unassign Courses</button>

            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><b>Unassign All Courses !! </b></h4>
                        </div>
                        <div class="modal-body">
                            <p style="color: #ac2925">Do you really want to unassign all course!!.</p>
                        </div>
                        <?php
                        if (isset($_GET["action"]) && $_GET["action"] == "unassign"){
                            $unasn->unAssign();
                        }
                        ?>
                        <div class="modal-footer">
                            <a href="?action=unassign" type="button" class="btn
                            btn-default">Yes</a>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

    </section>
        </div>

</div>

<?php include "footer.php"?>
