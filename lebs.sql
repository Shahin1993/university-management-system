-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2017 at 06:11 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lebs`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `c_code` varchar(255) NOT NULL,
  `c_name` varchar(255) NOT NULL,
  `c_credit` float NOT NULL,
  `c_description` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `semester_id` int(25) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `create_at` date NOT NULL,
  `update_at` date NOT NULL,
  `delete_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `c_code`, `c_name`, `c_credit`, `c_description`, `department_id`, `semester_id`, `status`, `create_at`, `update_at`, `delete_at`) VALUES
(27, 'MATH-32', 'Math 1', 3, 'sssd', 19, 0, 0, '2017-05-10', '0000-00-00', '0000-00-00'),
(29, 'BANG-123', 'Bangla 32', 2, 'we', 26, 0, 0, '2017-05-11', '0000-00-00', '0000-00-00'),
(33, 'CSE-223', 'English', 3, 'eee', 6, 2, 1, '2017-05-12', '2017-05-15', '0000-00-00'),
(34, 'Bang-23213', 'Bangla 32', 2, '22', 6, 3, 1, '2017-05-12', '2017-05-15', '0000-00-00'),
(35, 'MATH -321', 'Math3', 2, 'nbvvc', 19, 4, 1, '2017-05-12', '2017-05-15', '0000-00-00'),
(36, 'PHY-2121', 'physics', 2, 'sdd', 26, 5, 1, '2017-05-12', '2017-05-15', '0000-00-00'),
(37, 'CSE-2235', 'Computer Science', 2.5, 'fgh', 19, 6, 1, '2017-05-12', '2017-05-15', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `course_class_allocate`
--

CREATE TABLE IF NOT EXISTS `course_class_allocate` (
  `id` int(21) NOT NULL AUTO_INCREMENT,
  `department_id` int(25) NOT NULL,
  `course_id` int(25) NOT NULL,
  `room_id` int(23) NOT NULL,
  `day_id` int(25) NOT NULL,
  `start_time` varchar(233) NOT NULL,
  `end_time` varchar(233) NOT NULL,
  `value` tinyint(4) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `course_class_allocate`
--

INSERT INTO `course_class_allocate` (`id`, `department_id`, `course_id`, `room_id`, `day_id`, `start_time`, `end_time`, `value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(42, 19, 2, 1, 1, '10:23 PM', '10:30 PM', 1, '2017-05-07', '0000-00-00', '0000-00-00'),
(43, 19, 8, 1, 1, '10:22 PM', '10:30 PM', 1, '2017-05-07', '0000-00-00', '0000-00-00'),
(44, 19, 2, 1, 4, '10:30 PM', '10:30 PM', 1, '2017-05-07', '0000-00-00', '0000-00-00'),
(45, 19, 2, 2, 4, '10:30 PM', '10:30 PM', 1, '2017-05-07', '0000-00-00', '0000-00-00'),
(46, 19, 2, 4, 4, '10:30 PM', '10:30 PM', 1, '2017-05-07', '0000-00-00', '0000-00-00'),
(47, 19, 8, 1, 4, '10:31 PM', '10:30 PM', 1, '2017-05-07', '0000-00-00', '0000-00-00'),
(48, 19, 2, 1, 4, '10:21 PM', '10:21 PM', 1, '2017-05-07', '0000-00-00', '0000-00-00'),
(67, 6, 24, 2, 4, '12:45 PM', '1:03 PM', 1, '2017-05-09', '0000-00-00', '0000-00-00'),
(68, 6, 18, 1, 1, '12:45 AM', '1:45 AM', 1, '2017-05-09', '0000-00-00', '0000-00-00'),
(69, 6, 26, 3, 4, '1:30 AM', '3:30 AM', 1, '2017-05-10', '0000-00-00', '0000-00-00'),
(70, 6, 32, 2, 3, '4:15 PM', '5:15 PM', 1, '2017-05-12', '0000-00-00', '0000-00-00'),
(71, 6, 26, 2, 2, '4:15 PM', '5:15 PM', 1, '2017-05-12', '0000-00-00', '0000-00-00'),
(72, 6, 26, 2, 1, '7:45 PM', '8:45 PM', 0, '2017-05-12', '0000-00-00', '0000-00-00'),
(73, 19, 27, 1, 1, '7:45 PM', '10:45 PM', 0, '2017-05-12', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `course_student`
--

CREATE TABLE IF NOT EXISTS `course_student` (
  `id` int(21) NOT NULL AUTO_INCREMENT,
  `student_reg_no` varchar(25) NOT NULL,
  `course_id` int(25) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `course_student`
--

INSERT INTO `course_student` (`id`, `student_reg_no`, `course_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '40', 7, '0000-00-00', '0000-00-00', '0000-00-00'),
(2, '41', 8, '05/05/2017', '0000-00-00', '0000-00-00'),
(3, '40', 2, '27/05/2017', '0000-00-00', '0000-00-00'),
(4, '52', 26, '11/05/2017', '0000-00-00', '0000-00-00'),
(5, '53', 25, '16/05/2017', '0000-00-00', '0000-00-00'),
(6, '53', 25, '29/05/2017', '0000-00-00', '0000-00-00'),
(7, '53', 25, '08/05/2017', '0000-00-00', '0000-00-00'),
(8, '54', 25, '28/05/2017', '0000-00-00', '0000-00-00'),
(9, '55', 29, '22/05/2017', '0000-00-00', '0000-00-00'),
(10, '57', 25, '30/05/2017', '0000-00-00', '0000-00-00'),
(11, '58', 26, '24/05/2017', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `course_teacher`
--

CREATE TABLE IF NOT EXISTS `course_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `course_code` varchar(255) NOT NULL,
  `course_credit` float NOT NULL,
  `stat` tinyint(4) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `delet_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `course_teacher`
--

INSERT INTO `course_teacher` (`id`, `department_id`, `teacher_id`, `course_code`, `course_credit`, `stat`, `created_at`, `updated_at`, `delet_at`) VALUES
(1, 6, 26, 'CSE-2233', 0.5, 1, '2017-05-03', '0000-00-00', '0000-00-00'),
(2, 6, 26, 'CSE-2233', 0.5, 1, '2017-05-03', '0000-00-00', '0000-00-00'),
(3, 19, 24, '10', 2.5, 1, '2017-05-04', '0000-00-00', '0000-00-00'),
(4, 19, 25, '13', 2.5, 1, '2017-05-06', '0000-00-00', '0000-00-00'),
(5, 19, 24, '2', 2.5, 1, '2017-05-07', '0000-00-00', '0000-00-00'),
(6, 19, 24, '8', 3.4, 1, '2017-05-08', '0000-00-00', '0000-00-00'),
(7, 6, 26, '18', 3.3, 1, '2017-05-08', '0000-00-00', '0000-00-00'),
(8, 6, 26, '18', 3.3, 1, '2017-05-09', '0000-00-00', '0000-00-00'),
(9, 19, 24, '2', 2.5, 1, '2017-05-09', '0000-00-00', '0000-00-00'),
(10, 6, 26, '19', 3.3, 1, '2017-05-09', '0000-00-00', '0000-00-00'),
(11, 19, 28, '8', 3.4, 1, '2017-05-09', '0000-00-00', '0000-00-00'),
(12, 6, 26, '20', 2, 1, '2017-05-09', '0000-00-00', '0000-00-00'),
(13, 6, 26, '19', 3.3, 1, '2017-05-09', '0000-00-00', '0000-00-00'),
(14, 6, 27, '18', 3.5, 1, '2017-05-09', '0000-00-00', '0000-00-00'),
(15, 6, 26, '20', 2, 1, '2017-05-09', '0000-00-00', '0000-00-00'),
(16, 26, 31, '17', 2.5, 1, '2017-05-10', '0000-00-00', '0000-00-00'),
(17, 19, 28, '2', 2.5, 1, '2017-05-10', '0000-00-00', '0000-00-00'),
(18, 6, 30, '26', 2, 1, '2017-05-10', '0000-00-00', '0000-00-00'),
(19, 19, 28, '25', 3, 1, '2017-05-10', '0000-00-00', '0000-00-00'),
(20, 19, 28, '27', 3, 1, '2017-05-10', '0000-00-00', '0000-00-00'),
(21, 19, 28, '25', 3, 1, '2017-05-12', '0000-00-00', '0000-00-00'),
(22, 19, 28, '28', 2, 1, '2017-05-12', '0000-00-00', '0000-00-00'),
(23, 6, 33, '32', 2, 1, '2017-05-12', '0000-00-00', '0000-00-00'),
(24, 6, 33, '26', 2, 1, '2017-05-12', '0000-00-00', '0000-00-00'),
(25, 6, 30, '34', 2, 1, '2017-05-12', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE IF NOT EXISTS `days` (
  `id` int(21) NOT NULL AUTO_INCREMENT,
  `day` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `day`) VALUES
(1, 'Saturday'),
(2, 'Sunday'),
(3, 'Monday'),
(4, 'Tuesday'),
(5, 'Wednesday'),
(6, 'Thursday'),
(7, 'Friday');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `d_code` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `value` tinyint(4) NOT NULL,
  `create_at` date NOT NULL,
  `update_at` date NOT NULL,
  `delete_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `d_code`, `department`, `value`, `create_at`, `update_at`, `delete_at`) VALUES
(6, 'EEE-124', 'Electrical Engineering', 0, '2017-04-27', '0000-00-00', '0000-00-00'),
(19, 'CSE-121', 'Computer Science & Engineering', 0, '2017-04-28', '0000-00-00', '0000-00-00'),
(26, 'BBA-234', 'Bachelor of Business Administration', 0, '2017-05-03', '0000-00-00', '0000-00-00'),
(27, 'ENG-234', 'Bsic English', 1, '2017-05-09', '0000-00-00', '0000-00-00'),
(28, 'weee', 'wqerer', 1, '2017-05-10', '0000-00-00', '0000-00-00'),
(29, 'wewe', 'wewew', 1, '2017-05-10', '0000-00-00', '0000-00-00'),
(30, '3434', 'ewrerer', 1, '2017-05-10', '0000-00-00', '0000-00-00'),
(31, 'ttt32', 'sss', 1, '2017-05-10', '0000-00-00', '0000-00-00'),
(32, 'ttt32', 'ddd', 1, '2017-05-10', '0000-00-00', '0000-00-00'),
(33, 'ewewe', 'rtrtreer', 1, '2017-05-10', '0000-00-00', '0000-00-00'),
(34, 'ttr566', 'trfghjughiooo', 1, '2017-05-12', '0000-00-00', '0000-00-00'),
(35, 'zdgfdg', 'sdfgfddfg', 0, '2017-05-12', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE IF NOT EXISTS `designations` (
  `id` int(21) NOT NULL AUTO_INCREMENT,
  `designation` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `designation`) VALUES
(1, 'Professor'),
(2, 'Associate Professor'),
(3, 'Assistant Professor'),
(4, 'Senior Lecturer'),
(5, 'Lecturer');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE IF NOT EXISTS `grades` (
  `id` int(21) NOT NULL AUTO_INCREMENT,
  `grade` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `grade`) VALUES
(1, 'A+'),
(2, 'A'),
(3, 'A-'),
(4, 'B+'),
(5, 'B'),
(6, 'B-'),
(7, 'C+'),
(8, 'C'),
(9, 'C-'),
(10, 'D+'),
(11, 'D'),
(12, 'D-');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`) VALUES
(1, 'admin', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE IF NOT EXISTS `result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_reg_no` int(11) NOT NULL,
  `course_id` varchar(255) NOT NULL,
  `grade_id` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_reg_no` (`student_reg_no`),
  KEY `course_id` (`course_id`),
  KEY `grade_id` (`grade_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `student_reg_no`, `course_id`, `grade_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(23, 40, '21', 'A+', '2017-05-09 08:05:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 40, '17', 'A', '2017-05-09 09:05:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 41, '22', 'C+', '2017-05-09 09:05:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 42, '21', 'B+', '2017-05-09 09:05:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 53, '27', 'B+', '2017-05-11 07:05:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 55, '29', 'B+', '2017-05-11 01:05:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 57, '25', 'D+', '2017-05-12 11:05:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 58, '26', 'C', '2017-05-12 03:05:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(21) NOT NULL AUTO_INCREMENT,
  `room_no` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room_no`) VALUES
(1, 'R-101'),
(2, 'R-102'),
(3, 'R-201'),
(4, 'R-202'),
(5, 'R-301');

-- --------------------------------------------------------

--
-- Table structure for table `semesters`
--

CREATE TABLE IF NOT EXISTS `semesters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `semester` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `semesters`
--

INSERT INTO `semesters` (`id`, `semester`) VALUES
(1, 'First Semester'),
(2, 'Second Semester'),
(3, 'Third Semester'),
(4, 'Four Semester'),
(5, 'Five Semester'),
(6, 'Six Smester'),
(7, 'Seven Semester'),
(8, 'Eight Semester');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(21) NOT NULL AUTO_INCREMENT,
  `st_name` varchar(255) NOT NULL,
  `st_email` varchar(255) NOT NULL,
  `st_contact` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `st_address` varchar(255) NOT NULL,
  `st_department` varchar(233) NOT NULL,
  `stu_id` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `create_at` date NOT NULL,
  `update_at` date NOT NULL,
  `delete_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `st_name`, `st_email`, `st_contact`, `date`, `st_address`, `st_department`, `stu_id`, `status`, `create_at`, `update_at`, `delete_at`) VALUES
(40, 'Ebrahim', 'edrahim@gmail.com', '2147483647', '24/05/2017', 'qw', 'CSE-121', 'CSE-2017-001', 0, '2017-05-03', '2017-05-07', '0000-00-00'),
(41, 'Ebrahim', 'edrahim@gmail.com', '2147483647', '23/05/2017', 'sa', 'EEE-124', 'EEE-2017-001', 0, '2017-05-03', '0000-00-00', '0000-00-00'),
(42, 'Likhon', 'likhon@gmail.com', '2147483647', '27/05/2017', 'asd', 'EEE-124', 'EEE-2017-002', 0, '2017-05-03', '2017-05-10', '0000-00-00'),
(43, 'Ebrahim', 'edrsahim@gmail.com', '1923178433', '30/06/2017', 'k', 'BBA-234', 'EEE-2017-003', 0, '2017-05-03', '2017-05-07', '0000-00-00'),
(44, 'Ebrahim', 'edrasaddhim@gmail.com', '2147483647', '30/05/2017', '324324', 'CSE-121', 'CSE-2017-002', 0, '2017-05-10', '2017-05-10', '0000-00-00'),
(45, 'Ebrahim', 'edrahim@gmail.com', '2147483647', '29/05/2017', 'asas', 'CSE-121', 'CSE-2017-001', 0, '2017-05-10', '0000-00-00', '0000-00-00'),
(46, 'shahin', 'shahin@gmail.com', '2147483647', '22/05/2017', 'asa', 'EEE-124', 'EEE-2017-002', 0, '2017-05-10', '2017-05-10', '0000-00-00'),
(47, 'MD Emtiaz mahmud', 'emtiaz@gmail.com', '2147483647', '28/05/2017', 'ad', 'BBA-234', 'BBA-2017-002', 0, '2017-05-10', '0000-00-00', '0000-00-00'),
(48, 'MD Emtiaz mahmud', 'emtiaz@gmail.com', '2147483647', '28/05/2017', 'ssd', 'CSE-121', 'CSE-2017-002', 0, '2017-05-10', '2017-05-10', '0000-00-00'),
(49, 'Ebrahim', 'edrsahim@gmail.com', '2147483647', '29/05/2017', 'sd', 'BBA-234', 'BBA-2017-001', 0, '2017-05-10', '0000-00-00', '0000-00-00'),
(50, 'Ebrahim', 'edrdsahim@gmail.com', '2147483647', '29/05/2017', 'dd', 'CSE-121', 'CSE-2017-005', 0, '2017-05-10', '0000-00-00', '0000-00-00'),
(51, 'shahin', 'emtqiaz@gmail.com', '2147483647', '29/05/2017', 'xZx', 'BBA-234', 'BBA-2017-004', 0, '2017-05-10', '2017-05-10', '0000-00-00'),
(52, 'shahin', 'shahin@gmail.com', '214748364788', '29/05/2017', 'dsfd', 'EEE-124', 'EEE-2017-004', 1, '2017-05-11', '2017-05-12', '0000-00-00'),
(53, 'Ebrahim', 'edrasahim@gmail.com', '2147483647', '29/05/2017', 'sdsd', 'CSE-121', 'CSE-2017-006', 1, '2017-05-11', '0000-00-00', '0000-00-00'),
(54, 'MD Emtiaz mahmud', 'emtiaz@gmail.com', '2147483647', '11/05/2017', 'sd', 'CSE-121', 'CSE-2017-007', 1, '2017-05-11', '0000-00-00', '0000-00-00'),
(55, 'Likhon', 'likhon@gmail.com', '01923178432', '28/05/2017', 'ss', 'BBA-234', 'BBA-2017-005', 1, '2017-05-11', '2017-05-12', '0000-00-00'),
(56, 'Ebrahim', 'shashin@gmail.com', '01832258644', '29/05/2017', 'qwe', 'EEE-124', 'EEE-2017-005', 1, '2017-05-12', '2017-05-12', '0000-00-00'),
(57, 'Likhon', 'edrasahim@gmail.comcvbn', '019231784332', '23/05/2017', 'cvbn', 'CSE-121', 'CSE-2017-008', 1, '2017-05-12', '2017-05-12', '0000-00-00'),
(58, 'Ebrahim', 'edrahim@gmail.com', '019231784332', '28/05/2017', 'dfgh', 'EEE-124', 'EEE-2017-006', 1, '2017-05-12', '2017-05-12', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE IF NOT EXISTS `teachers` (
  `t_id` int(28) NOT NULL AUTO_INCREMENT,
  `t_name` varchar(255) NOT NULL,
  `t_address` varchar(255) NOT NULL,
  `t_email` varchar(255) NOT NULL,
  `t_contact` varchar(255) NOT NULL,
  `t_designation` int(25) NOT NULL,
  `t_department` int(25) NOT NULL,
  `t_credit` float NOT NULL,
  `status` tinyint(4) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL,
  PRIMARY KEY (`t_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`t_id`, `t_name`, `t_address`, `t_email`, `t_contact`, `t_designation`, `t_department`, `t_credit`, `status`, `create_at`, `update_at`, `delete_at`) VALUES
(24, 'Shahin Hossen', 'AS', 'shaDhin@gmail.com', '122', 1, 19, 32, 1, '2017-04-29 09:04:53', '2017-05-05 12:05:39', '0000-00-00 00:00:00'),
(26, 'Ajmol', 'as', 'ajmol@gmail.com', '1832258644', 1, 6, 12, 1, '2017-04-30 05:04:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'asa', 'asa', 'shahin@gamil.com', '1832258644', 2, 6, 23, 1, '2017-05-01 07:05:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Sumon Mahmud', 'as', 'sumon@gmail.com', '01832258644', 1, 19, 23, 0, '2017-05-02 05:05:58', '2017-05-12 11:05:22', '0000-00-00 00:00:00'),
(29, 'Abdullah', 'S', 'shahsin@gamil.com', '1832258644', 1, 4, 12, 1, '2017-05-02 07:05:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'Abdullah', 'cx', 'shachin@gamil.com', '1832258644', 2, 6, 12, 0, '2017-05-02 07:05:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'Abdullah', 'sd', 'shahin@gamil.comsd', '01832258644', 2, 26, 2.5, 0, '2017-05-02 07:05:25', '2017-05-12 11:05:55', '0000-00-00 00:00:00'),
(32, 'ffffffffffffffffffff', 'ADSF', 'shahcsin@gamil.com', '1832258644', 2, 26, 21, 1, '2017-05-12 10:05:03', '2017-05-12 10:05:43', '0000-00-00 00:00:00'),
(33, 'ffffffffffffffffffffhfhfgfdssdfghgf', 'xxcvbn', 'sumon@gmail.comgh', '01832258644', 1, 6, 34, 0, '2017-05-12 11:05:31', '2017-05-12 03:05:35', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
