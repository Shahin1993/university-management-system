<?php
namespace App\update;
use App\database\database;
session_start();
 class courseUpdate
 {
     private $c_code;
     private $c_name;
     private $c_credit;
     private $c_description;
     private $department_id;
     private $semester_id;
     private $c_id;

     public function addCourse($data1 = '')
     {

         $this->c_code = $data1['c_code'];
         $this->c_name = $data1['c_name'];
         $this->c_credit = $data1['c_credit'];
         $this->c_description = $data1['c_description'];
         $this->department_id = $data1['department_id'];
         $this->semester_id = $data1['semester_id'];
         $this->c_id=$data1['c_id'];

         return $this;
     }

     public function courseUpdate()
     {
         $db = database::getInstance();
         $query = "UPDATE courses SET c_code=:cod, c_name=:cnm, c_credit=:crd, c_description=:cds, department_id=:dpt, semester_id=:sem, update_at=:upd WHERE id=$this->c_id";
         $stmt = $db->prepare($query);
         $status = $stmt->execute(
             array(
                 ':cod'=> $this->c_code,
                 ':cnm' => $this->c_name,
                 ':crd'=>$this->c_credit,
                 ':cds'=>$this->c_description,
                 ':dpt'=>$this->department_id,
                 ':sem'=>$this->semester_id,

                ':upd' => date('Y-m-d'),
             ));
         if ($status) {
             $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size: 20px;'>Update successfully.</div>";

             header("location:../pages/view_ajaxCourse.php");
         } else {
             echo "something is wrong";
         }

     }
 }

?>