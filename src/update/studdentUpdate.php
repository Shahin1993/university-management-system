<?php

namespace App\update;

use App\database\database;
session_start();
class studdentUpdate
{
    private $st_name;
    private $st_email;
    private $st_contact;
    private $st_date;
    private $st_address;
    private $st_department;
    private $st_id;

    public function addStudent($data = '')
    {
        $this->st_name = $data['st_name'];
        $this->st_email = $data['st_email'];
        $this->st_contact = $data['st_contact'];
        $this->st_date = $data['date'];
        $this->st_address = $data['st_address'];
        $this->st_department = $data['st_department'];
        $this->st_id = $data['st_id'];
        return $this;
    }

    public function UpdateStudent()
    {

                $db = database::getInstance();
                $query = "UPDATE students SET st_name=:stn, st_email=:ste, st_contact=:stc, date=:d, st_address=:stad, st_department=:stdpt, update_at=:upd WHERE id=$this->st_id";
                $stmt = $db->prepare($query);

                $status = $stmt->execute(
                    array(
                        ':stn' => $this->st_name,
                        ':ste' => $this->st_email,
                        ':stc' => $this->st_contact,
                        ':d' => $this->st_date,
                        ':stad' => $this->st_address,
                        ':stdpt' => $this->st_department,
                        ':upd' => date('Y-m-d'),
                    ));
                if ($status) {

                    $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size:18px;'>Update Successfuly</div>";

                    header("location: ../pages/view_ajaxstudent.php");

                } else {
                    echo "something is wrong";
                }

            }
}
?>