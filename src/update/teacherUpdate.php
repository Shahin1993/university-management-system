<?php


namespace App\update;
use App\database\database;
session_start();
class teacherUpdate
{
    private $t_name;
    private $t_address;
    private $t_email;
    private $t_contact;
    private $t_designation;
    private $t_department;
    private $t_credit;
    private $tech_id;

    public function addTeacher($data = '')
    {

        $this->t_name = $data['t_name'];
        $this->t_address = $data['t_address'];
        $this->t_email = $data['t_email'];
        $this->t_contact = $data['t_contact'];
        $this->t_designation = $data['t_designation'];
        $this->t_department = $data['t_department'];
        $this->t_credit = $data['t_credit'];
        $this->tech_id = $data['tech_id'];

        return $this;

    }

    public function teacherUpdate()
    {



                $db = database::getInstance();
                $query = "UPDATE teachers SET t_name=:tn, t_address=:tad, t_email=:te, t_contact=:tct, t_designation=:tds, t_department=:tdpt, t_credit=:tcrd, update_at=:upd WHERE t_id=$this->tech_id";
                $stmt = $db->prepare($query);
                $status = $stmt->execute(
                    array(
                        ':tn' => $this->t_name,
                        ':tad' => $this->t_address,
                        ':te' => $this->t_email,
                        ':tct' => $this->t_contact,
                        ':tds' => $this->t_designation,
                        ':tdpt' => $this->t_department,
                        ':tcrd' => $this->t_credit,

                        ':upd' => date('Y-m-d h:m:s'),
                    ));
                if ($status) {
                    $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size: 30px;'>Update successfully.</div>";

                    header("location:../pages/view_ajaxTeacher.php");
                } else {
                    echo "something is wrong";
                }

            }
}
