<?php

 namespace App\course;
 //use PDO;
 use App\database\database;
 session_start();
class course
{

    private $c_code;
    private $c_name;
    private $c_credit;
    private $c_description;
    private $department_id;
    private $semester_id;
    private $c_id;

    public function addCourse($data1 = '')
    {

        $this->c_code = $data1['c_code'];
        $this->c_name = $data1['c_name'];
        $this->c_credit = $data1['c_credit'];
        $this->c_description = $data1['c_description'];
        $this->department_id = $data1['department_id'];
        $this->semester_id = $data1['semester_id'];
        $this->c_id=$data1['c_id'];

        return $this;
    }
    public function storeCourse()
    {
        if ($_POST) {
            if (empty($this->c_code)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Course code cannot be empty</div>";
            } elseif (empty($this->c_name)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Course name cannot be empty</div>";
            } elseif (empty($this->c_credit)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Credit cannot be empty</div>";
            } elseif (empty($this->c_description)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Description cannot be empty</div>";
            } elseif (empty($this->department_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Please Select a department name..</div>";
            } elseif (empty($this->semester_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'> Please Select a semester ..</div>";
            } elseif (strlen($this->c_code) < 5) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>This code is too short ! Please type a code minimum 5 charachters</div>";
            }
            elseif ($this->c_credit<.5 or $this->c_credit>5) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Credit must be 0.5 to 5 </div>";
            }
            else {
                $db = database::getInstance();
                $statement = $db->prepare("SELECT * FROM courses WHERE c_code=? AND status=1");
                $statement->execute(array($this->c_code));
                $total = $statement->rowCount();


                if ($total > 0) {
                    $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px'>Course Code is already exsist! Please type another code</div>";
                } else {
                    $db = database::getInstance();
                    $stmt = $db->prepare("SELECT * FROM courses WHERE c_name=?  AND status=1");
                    $stmt->execute(array($this->c_name));
                    $count = $stmt->rowCount();

                    if ($count > 0) {
                        $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px'>Course Name is already exsist! Please try another Course name !!</div>";
                    } else {

                        $db = database::getInstance();
                        $sql = "INSERT INTO courses(c_code,c_name,c_credit,c_description,department_id,semester_id,create_at) values(:c, :n, :cr, :des, :dpt, :sem, :d)";
                        $stmt = $db->prepare($sql);
                        $status = $stmt->execute(
                            array(
                                ':c' => $this->c_code,
                                ':n' => $this->c_name,
                                ':cr' => $this->c_credit,
                                ':des' => $this->c_description,
                                ':dpt' => $this->department_id,
                                ':sem' => $this->semester_id,
                                ':d' => date('Y-m-d'),
                            ));
                        if ($status) {
                            $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size: 30px;'>Corse add successfully.</div>";

                            //  header("location: ../pages/course.php");
                        } else {
                            echo "something is wrong";
                        }
                    }
                }
            }
        }
    }




    public function viewAllcourse()
    {
        $db = database::getInstance();

        $query = "SELECT * FROM `courses` WHERE status=1 ORDER BY id DESC ";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }

    public function viewcourse($id='')
    {
        $db = database::getInstance();

        $query = "SELECT * FROM `courses` WHERE id=?  AND status=1";
        $stmt = $db->prepare($query);
        $stmt->execute(array($id));
        $data2 = $stmt->fetch();
        return $data2;
    }

    public function delete($id='')
    {
        $db = database::getInstance();
        $statement= $db->prepare("DELETE FROM courses WHERE id=? ");
        $statement->execute(array($id));

        header("location: view_course.php");
    }
    public function softDeleteCourse($id=''){
        $db = database::getInstance();
        $sql = "UPDATE courses SET status='0' WHERE id=?";
        $stmt = $db->Prepare($sql);
        $status =  $stmt->execute(array($id));
        if ($status){
            $_SESSION["Message"] = "<div style='color: green;font-weight: bold;font-size: 20px;'>Deleted successfully.</div>";
            header("location: view_ajaxCourse.php");
        }else{
            $_SESSION["Message"] = "failed to unassign.";
        }
    }

    public function getCourseAjax($id)
    {
        try {
            //  $pdo = Database::getInstance();
            $db = database::getInstance();
            $query = $db->prepare('SELECT * FROM courses WHERE id = :id AND status=1');
            $query->execute(array(':id' => $id));
            $data=$query->fetch();
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $data;
    }


}