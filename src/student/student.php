<?php

namespace App\student;
use App\database\database;
session_start();
class student
{
    private $st_name;
    private $st_email;
    private $st_contact;
    private $st_date;
    private $st_address;
    private $st_department;
   // private $stu_id;

    public function addStudent($data='')
    {
        $this->st_name=$data['st_name'];
        $this->st_email=$data['st_email'];
        $this->st_contact=$data['st_contact'];
        $this->st_date=$data['date'];
        $this->st_address=$data['st_address'];
        $this->st_department=$data['st_department'];
           //$this->stu_id=$data['stu_id'];
        return $this;
    }

    public function getstudent()
    {
        if ($_POST) {
            if (empty($this->st_name)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Please insert a student name</div>";
            } elseif (empty($this->st_email)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Please insert a email</div>";
            } elseif (empty($this->st_contact)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Apnar contact number deowa hoy ni!!!</div>";
            } elseif (empty($this->st_address)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Address likh te vule gecen!!!</div>";
            } elseif (empty($this->st_department)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Please Select a department name..</div>";
            } elseif (strlen($this->st_contact) < 11) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size: 15px;font-size:18px;'>Contact Number Must be 11 charecters</div>";
            } else {
                $db = database::getInstance();
                $statement = $db->prepare("SELECT * FROM students WHERE st_email=? AND status=1");
                $statement->execute(array($this->st_email));
                $total = $statement->rowCount();
                if ($total > 0) {
                    $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>OOPS!!! This email is already exsist! Please type another email..</div>";
                } else {
                    $db = database::getInstance();
                    $sql = "INSERT INTO students(st_name,st_email,st_contact,date,st_address,st_department,stu_id,  create_at) values(:stn, :ste, :stc, :d, :stad, :stdpt, :stu_id, :cd)";
                    $stmt = $db->prepare($sql);
                    if (!empty($this->st_department)) {
                        $numofRows = $db->query("SELECT * FROM students WHERE st_department='$this->st_department'")->rowCount();
                        $nRows = $numofRows + 1;
                        if ($nRows < 10) {
                            $stu_id = substr($this->st_department, 0, 3) . "-" . date('Y') . "-" . "00" . $nRows;
                        } elseif ($nRows >= 10 && $nRows <= 99) {
                            $stu_id = substr($this->st_department, 0, 3) . "-" . date('Y') . "-" . "0" . $nRows;
                        } elseif ($nRows >= 100) {
                            $stu_id = substr($this->st_department, 0, 3) . "-" . date('Y') . "-" . $nRows;
                        }
                    }

                    $status = $stmt->execute(
                        array(
                            ':stn' => $this->st_name,
                            ':ste' => $this->st_email,
                            ':stc' => $this->st_contact,
                            ':d' => $this->st_date,
                            ':stad' => $this->st_address,
                            ':stdpt' => $this->st_department,
                            ':stu_id' => $stu_id,
                            ':cd' => date('Y-m-d'),
                        ));
                    if ($status) {


                        $db = database::getInstance();
                        $query = " SELECT stu_id FROM students ORDER BY id DESC ";
                        $stmt = $db->prepare($query);
                        $stmt->execute();
                        $data = $stmt->fetch();

                        $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size:18px;'>Student Registation Number is</div>"."<div style='color: blue;font-weight: bold;font-size:16;'>".$data['stu_id']."</div>";

                      //  header("location: ../student_register.php");

                    } else {
                        echo "something is wrong";
                    }

                }
            }
        }

    }

    public function viewAllstudent()
    {
        $db = database::getInstance();

        $query = "SELECT * FROM `students` WHERE status=1";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
    public function delete($id='')
    {
        $db = database::getInstance();
        $statement= $db->prepare("DELETE FROM students WHERE id=? ");
        $statement->execute(array($id));

        header("location: view_student.php");
    }
    public function softDeleteStudent($id=''){
        $db = database::getInstance();
        $sql = "UPDATE students SET status='0' WHERE id=?";
        $stmt = $db->Prepare($sql);
        $status =  $stmt->execute(array($id));
        if ($status){
            header("location:view_ajaxstudent.php");
            $_SESSION["Message"] = "<div style='color: green;font-weight: bold;font-size: 20px;'>Deleted successfully.</div>";

        }else{
            $_SESSION["Message"] = "failed to delete.";
        }
    }


    public function showStudent_id()
    {
        //$db = new PDO('mysql:host=localhost;dbname=lebs', 'root', '');
        $db = database::getInstance();
        $query = "SELECT * FROM `students` WHERE status=1";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $data2 = $stmt->fetchAll();
        return $data2;
    }

    public function showStudent($id='')
    {
        //$db = new PDO('mysql:host=localhost;dbname=lebs', 'root', '');
        $db = database::getInstance();
        $query = "SELECT * FROM `students` WHERE id=$id AND status=1";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $data2 = $stmt->fetch();
        return $data2;
    }

    public function getStudentAjax($id)
    {

            //  $pdo = Database::getInstance();
            $db = database::getInstance();
            $query = $db->prepare('SELECT * FROM students LEFT JOIN departments on students.st_department=departments.d_code WHERE students.id = :id AND students.status=1');
            $query->execute(array(':id' => $id));
            $data=$query->fetch();


        $query1 = "SELECT * FROM courses WHERE department_id = '".$data["id"]."' AND status=1";
        $stmt1 = $db->Prepare($query1);
        $stmt1->execute();
        $data3 =  $stmt1->fetchAll();

        $data2=[
          'student'=>$data,
            'course'=>$data3,
        ];
        return $data2;
    }





}