<?php

 namespace App\enrollCourseStudent;
 use App\database\database;
 session_start();
class enrollCourseStudent
{
 private $stu_id;
 private $course_id;
 private $st_date;

 public function addCourseStudent($data='')
 {
     $this->stu_id=$data['stu_id'];
     $this->course_id=$data['course_id'];
     $this->st_date=$data['st_date'];
     return $this;
 }

    public function storeCourseStudent()
    {
        if ($_POST) {
            if (empty($this->stu_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>Please select a student registation number</div>";
                header("location:../pages/enroll_course.php");
            } elseif (empty($this->course_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>Please select a course</div>";
                header("location:../pages/enroll_course.php");
            } elseif (empty($this->st_date)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>Please insert date???</div>";
                header("location:../pages/enroll_course.php");
            } else {
                $db = database::getInstance();
                $stmt = $db->prepare("SELECT * FROM course_student WHERE student_reg_no=? AND course_id=?");
                $stmt->execute(array($this->stu_id, $this->course_id));
                $count = $stmt->rowCount();

                if ($count > 0) {
                    $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px'>This student is course  already Enrolled?Please select another course ..</div>";
                    header("location: ../pages/enroll_course.php");
                }


            else {
                    $db = database::getInstance();
                    $sql = "INSERT INTO course_student(student_reg_no,course_id,created_at) values(:stu_id, :crs, :d)";
                    $stmt = $db->prepare($sql);
                    $status = $stmt->execute(
                        array(
                            ':stu_id' => $this->stu_id,
                            ':crs' => $this->course_id,
                            ':d' => $this->st_date,
                        ));
                    if ($status) {

                        // $ticket_id=$db->lastInsertId();
                        //$_SESSION['Message']= " Your trouble ticket number is ". $ticket_id ;

                        $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size: 30px;'>Enroll Course successfully.</div>";

                        header("location:../pages/enroll_course.php");
                    } else {
                        echo "something is wrong";
                    }
                }
            }
        }
    }

}