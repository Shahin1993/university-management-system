<?php

 namespace App\courseAssign;
 use App\database\database;
 session_start();
class courseAssign
{
   private $department_id;
   private $teacher_id;
   private $course_code;
   private $course_credit;

   public function addcourseteacher($data='')
   {
       $this->department_id=$data['department_id'];
       $this->teacher_id=$data['teacher_id'];
       $this->course_code=$data['course_code'];
       $this->course_credit=$data['c_credit'];
       return $this;
   }
   public function getassignteacher()
   {
       if ($_POST) {
           if (empty($this->department_id)) {
               $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>Please select a department???</div>";
               header("location:../pages/courseAssign.php");
           } elseif (empty($this->teacher_id)) {
               $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>Please select a techer name???</div>";
               header("location:../pages/courseAssign.php");
           } elseif (empty($this->course_code)) {
               $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>Please select a course code???</div>";
               header("location:../pages/courseAssign.php");
           } else {

               $db = database::getInstance();
               $stmt = $db->prepare("SELECT * FROM course_teacher WHERE course_code=? AND stat=0");
               $stmt->execute(array($this->course_code));
               $count = $stmt->rowCount();

               if ($count > 0) {
                   $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>Course   is already assigned! Please try another course  !!</div>";
                   header("location:../pages/courseAssign.php");
               } else {
                   $db = database::getInstance();
                   $sql = "INSERT INTO course_teacher(department_id,teacher_id,course_code,course_credit,created_at) values(:dpt, :tch, :cod, :crd, :d)";
                   $stmt = $db->prepare($sql);
                   $status = $stmt->execute(
                       array(
                           ':dpt' => $this->department_id,
                           ':tch' => $this->teacher_id,
                           ':cod' => $this->course_code,
                           ':crd' => $this->course_credit,
                           ':d' => date('Y-m-d'),
                       ));
                   if ($status) {
                       $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size: 20px;'>Course Assign successfully.</div>";

                       header("location:../pages/courseAssign.php");
                   } else {
                       echo "something is wrong";
                   }
               }
           }
       }
   }
    public function getCourseAjax($id)
    {

            //  $pdo = Database::getInstance();
            $db = database::getInstance();
            $query = $db->prepare('SELECT * FROM courses WHERE department_id = :id AND status=1');
            $query->execute(array(':id' => $id));
            $course=$query->fetchAll();

        $query = $db->prepare('SELECT * FROM course_teacher WHERE department_id = :id AND stat=0');
        $query->execute(array(':id' => $id));
        $teacher=$query->fetchAll();
        $data32=[
            'course'=>$course,
            'teacher'=>$teacher,
        ];
        return $data32;
    }

    public function moveTo_trash($id){
        $this->courseId = $id;
        $db = database::getInstance();
        $sql = "UPDATE courses SET status='2' WHERE id=:id";
        $stmt = $db->Prepare($sql);
        $data = [
            ':id' =>$this->courseId
        ];
        $status =  $stmt->execute($data);
        if ($status){
            $_SESSION["Message"] = "Course moved to trash.";
            header("location: view_course_statics.php");
        }else{
            $_SESSION["Message"] = "Course move failed.";
        }
    }

    public static function unAssign(){
        $db = database::getInstance();
        $sql = "UPDATE course_teacher SET stat='1'";
        $stmt = $db->Prepare($sql);
        $status =  $stmt->execute();
        if ($status){
            $_SESSION["Message"] = "<div style='color: green;font-weight: bold;font-size: 20px;'>Unassigned all courses successfully.</div>";
            //header("Location: unassign.php");
        }else{
            $_SESSION["Message"] = "failed to unassign.";
        }
    }
}