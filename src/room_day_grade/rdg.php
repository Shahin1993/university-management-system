<?php

namespace App\room_day_grade;
use App\database\database;

class rdg
{
    public function getRoom()
    {
        $db = database::getInstance();
        $query = "SELECT * FROM `rooms` ORDER BY id ASC";
        $stmt =$db->prepare($query);
        $stmt->execute();
        $data1 = $stmt->fetchAll();

        return $data1;

    }
    public function getDay()
    {
        $db = database::getInstance();
        $query = "SELECT * FROM `days` ORDER BY id ASC";
        $stmt =$db->prepare($query);
        $stmt->execute();
        $data2 = $stmt->fetchAll();

        return $data2;

    }
    public function getGrade()
    {
        $db = database::getInstance();
        $query = "SELECT * FROM `grades` ORDER BY id ASC";
        $stmt =$db->prepare($query);
        $stmt->execute();
        $data3 = $stmt->fetchAll();

        return $data3;

    }
}