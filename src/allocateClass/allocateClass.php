<?php

 namespace App\allocateClass;
 use App\database\database;
 session_start();
class allocateClass
{

    private $department_id;
    private $course_id;
    private $room_id;
    private $day_id;
    private $start_time;
    private $end_time;

    public function addAlloctecourse($data1 = '')
    {

        $this->department_id = $data1['department_id'];
        $this->course_id = $data1['course_id'];
        $this->room_id = $data1['room_id'];
        $this->day_id = $data1['day_id'];
        $this->start_time = $data1['start_time'];
        $this->end_time = $data1['end_time'];


        return $this;
    }

    public function storeAllocateCourse()
    {
        if ($_POST) {
            if (empty($this->department_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Please Select a department name..</div>";
                header("location: ../pages/allocate_classrooms.php");
            } elseif (empty($this->course_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Course name cannot be empty</div>";
                header("location: ../pages/allocate_classrooms.php");
            } elseif (empty($this->room_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Please ekta room select koron;</div>";
                header("location: ../pages/allocate_classrooms.php");
            } elseif (empty($this->day_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Please 7 diner ek din to select korben?/</div>";
                header("location: ../pages/allocate_classrooms.php");
            } elseif (empty($this->start_time)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>Please insert start time..</div>";
                header("location: ../pages/allocate_classrooms.php");
            } elseif (empty($this->end_time)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'> Please insert end time ..</div>";
                header("location: ../pages/allocate_classrooms.php");
            }
            else {

                $db = database::getInstance();
                $stmt = $db->prepare("SELECT * FROM course_class_allocate WHERE day_id=? AND course_id=? AND value=0");
                $stmt->execute(array($this->day_id, $this->course_id));
                $count = $stmt->rowCount();

                if ($count > 0) {
                    $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px'>This day is course already  allocated!!</div>";
                    header("location: ../pages/allocate_classrooms.php");
                }

                        else {
                            $db = database::getInstance();
                            $sql = "SELECT * FROM course_class_allocate WHERE room_id = $this->room_id AND day_id =$this->day_id AND value=0";
                            $stmt =$db->Prepare($sql);
                            $stmt->execute();
                            $data = $stmt->fetchAll();
                           foreach ($data as $value) {
                               $databaseStart = $value["start_time"];
                               $databaseEnd = $value["end_time"];
                           }
                                if ($this->start_time >= $databaseStart && $this->start_time <= $databaseEnd ||
                                    $this->end_time >= $databaseStart && $this->end_time <= $databaseEnd ||
                                    $this->start_time <= $databaseStart && $this->end_time >= $databaseEnd
                                ) {


                                    $newStart = $databaseStart;
                                    $newEnd = $databaseEnd;
                                    $startTime = $newStart;
                                    $endTime = $newEnd;
                                    $_SESSION['Message']= "<div style='color:red;font-weight: bold;font-size:16px'>This room blocked for"."--" . $startTime . " to " . $endTime."</div>";
                                    header("location: ../pages/allocate_classrooms.php");
                                }



                    else {

                            $db = database::getInstance();
                            $sql = "INSERT INTO course_class_allocate(department_id,course_id,room_id,day_id,start_time,end_time,created_at) values(:dpt, :crd, :rm, :day, :str, :ed, :d)";
                            $stmt = $db->prepare($sql);
                            $status = $stmt->execute(
                                array(
                                    ':dpt' => $this->department_id,
                                    ':crd' => $this->course_id,
                                    ':rm' => $this->room_id,
                                    ':day' => $this->day_id,
                                    ':str' => $this->start_time,

                                    ':ed' => $this->end_time,
                                    ':d' => date('Y-m-d'),
                                ));
                            if ($status) {
                                $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size: 30px;'>Allocat Class roome  successfully.</div>";

                                header("location: ../pages/allocate_classrooms.php");
                            } else {
                                echo "something is wrong";
                            }
                        }
                    }

                }
            }
        }

    public function getClassAjax($id)
    {

        //  $pdo = Database::getInstance();
        $db = database::getInstance();
        $query = $db->prepare('SELECT * FROM course_class_allocate WHERE department_id = :id');
        $query->execute(array(':id' => $id));
        $class=$query->fetchAll();


        return $class;
    }


     public static function unAllocate (){
         $db = database::getInstance();
         $sql = "UPDATE course_class_allocate SET value='1'";
         $stmt = $db-> Prepare($sql);
         $status =  $stmt->execute();
         if ($status){
             $_SESSION["Message"] = "<div style='color: green;font-weight: bold;font-size: 20px;'>unallocated all rooms successfully.</div>";
            // header("Location: unallocate.php");
         }else{
             $_SESSION["Message"] = "failed to unallocate.";
         }
     }


}
