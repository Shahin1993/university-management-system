<?php

 namespace App\studentResult;
 use App\database\database;
 session_start();
class studentResult
{
    private $stu_id;
    private $course_id;
    private $grade_id;

    public function addStudentResult($data='')
    {
        $this->stu_id=$data['stu_id'];
        $this->course_id=$data['course_id'];
        $this->grade_id=$data['grade_id'];
        return $this;
    }

    public function storeStudentResult()
    {
        if ($_POST) {
            if (empty($this->stu_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>Please select a student registation number</div>";
                header("location:../pages/add_student_result.php");
            } elseif (empty($this->course_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>Please select a course code</div>";
                header("location:../pages/add_student_result.php");
            } elseif (empty($this->grade_id)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>Please insert date???</div>";
                header("location:../pages/add_student_result.php");
            } else {

                $db = database::getInstance();
                $stmt = $db->prepare("SELECT * FROM result WHERE student_reg_no=? AND course_id=?");
                $stmt->execute(array($this->stu_id, $this->course_id));
                $count = $stmt->rowCount();

                if ($count > 0) {
                    $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px'>This student and course result already save??!!</div>";
                    header("location: ../pages/add_student_result.php");
                } else {

                    $db = database::getInstance();
                    $sql = "INSERT INTO result(student_reg_no,course_id,grade_id,created_at) values(:stu_id, :crs, :grd, :d)";
                    $stmt = $db->prepare($sql);
                    $status = $stmt->execute(
                        array(
                            ':stu_id' => $this->stu_id,
                            ':crs' => $this->course_id,
                            ':grd' => $this->grade_id,
                            ':d' => date('Y-m-d h:m:s'),
                        ));
                    if ($status) {

                        // $ticket_id=$db->lastInsertId();
                        //$_SESSION['Message']= " Your trouble ticket number is ". $ticket_id ;

                        $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size: 30px;'>Result add successfully.</div>";

                        header("location:../pages/add_student_result.php");
                    } else {
                        echo "something is wrong";
                    }
                }
            }
        }

    }
    public function viewAllResult()
    {
        //$db = new PDO('mysql:host=localhost;dbname=lebs', 'root', '');
        $db = database::getInstance();
        $query = "SELECT * FROM `result` LEFT JOIN courses on result.course_id=courses.id";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
    public function viewResult($id='')
    {
        $db = database::getInstance();

        $query = "SELECT * FROM `result` WHERE id=?";
        $stmt = $db->prepare($query);
        $stmt->execute(array($id));
        $data2 = $stmt->fetch();
        return $data2;
    }


    public function EnrollStudentAjax($id)
    {

        //  $pdo = Database::getInstance();
        $db = database::getInstance();
        $query = $db->prepare('SELECT * FROM students LEFT JOIN departments on students.st_department=departments.d_code WHERE students.id = :id AND students.status=1');
        $query->execute(array(':id' => $id));
        $student1=$query->fetch();

        $db = database::getInstance();
        $query = $db->prepare('SELECT * FROM course_student LEFT JOIN courses on course_student.course_id=courses.id WHERE student_reg_no = :id');
        $query->execute(array(':id' => $id));
        $course=$query->fetchAll();
        $data2=[
            'student1'=>$student1,
            'course'=>$course,
        ];


        return $data2;
    }



    public function getStudentAjax($id)
    {

            //  $pdo = Database::getInstance();
            $db = database::getInstance();
            $query = $db->prepare('SELECT * FROM students LEFT JOIN departments on students.st_department=departments.d_code WHERE students.id = :id AND students.status=1');
            $query->execute(array(':id' => $id));
            $student=$query->fetch();

        $db = database::getInstance();
        $query = $db->prepare('SELECT * FROM result LEFT JOIN courses on result.course_id=courses.id WHERE student_reg_no = :id');
        $query->execute(array(':id' => $id));
        $grade=$query->fetchAll();
            $data=[
              'student'=>$student,
                'grade'=>$grade,
            ];


        return $data;
    }
}