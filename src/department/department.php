<?php

namespace App\department;
//include ('../database/database.php');
use App\database\database;
session_start();
class department
{
    private $d_code;
    private $department_n;
    private $d_id;

    public function addDepartment($data1 = '')
    {

        $this->d_code = $data1['d_code'];
        $this->department_n = $data1['department_n'];
        $this->d_id=$data1['d_id'];

        return $this;
    }
    public function storeDepartment()
    {
        if ($_POST) {
            if (empty($this->d_code)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>Department code can not be empty?</div>";
            } elseif (empty($this->department_n)) {
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>Department Name can not be empty?</div>";
            }
            elseif(strlen($this->d_code)<2){
                $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>This code is too short ! Please type a code between 2 and 7 charachters</div>";
            }
            else {
                $db = database::getInstance();
                $statement = $db->prepare("SELECT * FROM departments WHERE d_code=? AND value=0");
                $statement->execute(array($this->d_code));
                $total = $statement->rowCount();



               if ($total > 0) {
                    $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>Department Code is already exsist! Please type another code</div>";
                } else {
                   $db = database::getInstance();
                    $stmt = $db->prepare("SELECT * FROM departments WHERE department=? AND value=0");
                    $stmt->execute(array($this->department_n));
                    $count = $stmt->rowCount();

                    if ($count >0) {
                        $_SESSION['Message'] = "<div style='color:red;font-weight: bold;'>Department Name is already exsist! Please try another Department name !!</div>";
                    } else {
                        $db = database::getInstance();
                        $sql = "INSERT INTO departments(d_code,department,create_at) values(:c, :q, :d)";
                        $stmt = $db->prepare($sql);
                        $status = $stmt->execute(
                            array(
                                ':c' => $this->d_code,
                                ':q' => $this->department_n,
                                ':d' => date('Y-m-d'),
                            ));
                        if ($status) {

                           // $ticket_id=$db->lastInsertId();
                            //$_SESSION['Message']= " Your trouble ticket number is ". $ticket_id ;

                          $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size: 30px;'>Department add successfully.</div>";

                            //header("location:../pages/department.php");
                        } else {
                            echo "something is wrong";
                        }
                    }
                }
            }
        }
   }

    public function getDepartment()
    {
        //$db = new PDO('mysql:host=localhost;dbname=lebs', 'root', '');
        $db = database::getInstance();
        $query = "SELECT * FROM `departments` WHERE value=0";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
    public function view($id='')
    {

        $db = database::getInstance();
        $query = "SELECT * FROM `departments` WHERE id=$id AND value=0";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }
    public function update()
    {
        $db = database::getInstance();
        $query = "UPDATE departments SET d_code=:dc, department=:dpt WHERE id=$this->d_id AND value=0";
        $stmt = $db->prepare($query);
        $status = $stmt->execute(
            array(
                // ':u' => $this->username,
                ':dc' => $this->d_code,
                ':dpt'=>$this->department_n,

               // ':update_at' => date('Y-m-d h:m:s'),
            ));
        if ($status) {
            $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size: 20px;'>Update successfully.</div>";

           header("location:../pages/view_department.php");
        } else {
            echo "something is wrong";
        }

    }
    public function delete($id='')
    {
        $db = database::getInstance();
        $statement= $db->prepare("DELETE FROM departments WHERE id=?");
        $statement->execute(array($id));

        header("location: view_department.php");
    }

    public function getCourseAjax($id)
    {

          //  $pdo = Database::getInstance();
            $db = database::getInstance();
            $query = $db->prepare('SELECT * FROM teachers WHERE t_department = :id AND status=0');
            $query->execute(array(':id' => $id));
            $teachers=$query->fetchAll();


        $query = $db->prepare('SELECT * FROM courses WHERE department_id = :id AND status=1');
        $query->execute(array(':id' => $id));
        $course=$query->fetchAll();
        $data=[
          'teacher'=>$teachers,
            'course'=>$course,
        ];

        return $data;

    }




    public function getCourseAllocateAjax($id)
    {

        //  $pdo = Database::getInstance();
        $db = database::getInstance();
        $query = $db->prepare('SELECT * FROM courses WHERE department_id = :id AND status=1');
        $query->execute(array(':id' => $id));
        $course=$query->fetchAll();


        return $course;

    }


    public function softDelete($id=''){
        $db = database::getInstance();
        $sql = "UPDATE departments SET value='1' WHERE id=?";
        $stmt = $db->Prepare($sql);
        $status =  $stmt->execute(array($id));
        if ($status){
            $_SESSION["Message"] = "<div style='color: green;font-weight: bold;font-size: 20px;'>Deleted successfully.</div>";
            header("location: view_department.php");
        }else{
            $_SESSION["Message"] = "failed to delete.";
        }
    }



}
?>