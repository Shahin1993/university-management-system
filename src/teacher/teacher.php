<?php

 namespace App\teacher;
 use App\database\database;
 session_start();
class teacher
{
    private $t_name;
    private $t_address;
    private $t_email;
    private $t_contact;
    private $t_designation;
    private $t_department;
    private $t_credit;

    public function addTeacher($data = '')
    {

        $this->t_name = $data['t_name'];
        $this->t_address = $data['t_address'];
        $this->t_email = $data['t_email'];
        $this->t_contact= $data['t_contact'];
        $this->t_designation = $data['t_designation'];
        $this->t_department = $data['t_department'];
        $this->t_credit = $data['t_credit'];

        return $this;
    }
    public function storeTeacher()
    {
        if ($_POST) {
            if(empty($this->t_name)){
                $_SESSION['Message']="<div style='color:red;font-weight: bold;font-size:18px;'>Teacher name cannot be empty</div>";
            }
            elseif (empty($this->t_address)){
                $_SESSION['Message']="<div style='color:red;font-weight: bold;font-size:18px;'>Teacher Address cannot be empty</div>";
            }
            elseif (empty($this->t_email)){
                $_SESSION['Message']="<div style='color:red;font-weight: bold;font-size:18px;'>Teacher Email cannot be empty</div>";
            }
            elseif (empty($this->t_contact)){
                $_SESSION['Message']="<div style='color:red;font-weight: bold;font-size: 15px;font-size:18px;'>Contact Number cannot be empty</div>";
            }
            elseif (empty($this->t_designation)){
                $_SESSION['Message']="<div style='color:red;font-weight: bold;font-size: 15px;font-size:18px;'>Please select a designation name</div>";
            }
            elseif (strlen($this->t_contact)<11){
                $_SESSION['Message']="<div style='color:red;font-weight: bold;font-size: 15px;font-size:18px;'>Contact Number Must be 11 charecters</div>";
            }
         else {
             $db = database::getInstance();
             $statement = $db->prepare("SELECT * FROM teachers WHERE t_email=? AND status=0");
             $statement->execute(array($this->t_email));
             $total = $statement->rowCount();
             if ($total > 0) {
                 $_SESSION['Message'] = "<div style='color:red;font-weight: bold;font-size:18px;'>OOPS!!! This email is already exsist!</div>";
             } else {
                 $db = database::getInstance();
                 $sql = "INSERT INTO teachers(t_name,t_address,t_email,t_contact,t_designation,t_department,t_credit,create_at) values(:n, :a, :e, :c, :dsg, :dpt, :cr, :d)";
                 $stmt = $db->prepare($sql);
                 $status = $stmt->execute(
                     array(
                         ':n' => $this->t_name,
                         ':a' => $this->t_address,
                         ':e' => $this->t_email,
                         ':c' => $this->t_contact,
                         ':dsg' => $this->t_designation,
                         ':dpt' => $this->t_department,
                         ':cr' => $this->t_credit,
                         ':d' => date('Y-m-d h:m:s'),
                     ));
                 if ($status) {
                     $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size: 30px;'>Teacher add successfully.</div>";

                     //header("location:../pages/teacher.php");
                 } else {
                     //echo "something is wrong";
                     $_SESSION['Message']="wrong";
                 }
             }
         }
        }
    }
    public function viewAllteacher()
    {
        $db = database::getInstance();

        $query = "SELECT * FROM `teachers` WHERE status=0 ORDER BY t_id DESC";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }

    public function viewTeacher($id='')
    {
        $db = database::getInstance();

        $query = "SELECT * FROM `teachers` WHERE t_id=? AND status=0";
        $stmt = $db->prepare($query);
        $stmt->execute(array($id));
        $data2 = $stmt->fetch();
        return $data2;
    }

    public function delete($id='')
    {
        $db = database::getInstance();
        $statement= $db->prepare("DELETE FROM teachers WHERE t_id=?");
        $statement->execute(array($id));

        header("location: view_teacher.php");
    }
    public function getDesignation(){
        $db = database::getInstance();
        $query = "SELECT * FROM `designations`";
        $stmt =$db->prepare($query);
        $stmt->execute();
        $data5 = $stmt->fetchAll();

        return $data5;

    }
    public function softDeleteTeacher($id=''){
        $db = database::getInstance();
        $sql = "UPDATE teachers SET status='1' WHERE t_id=?";
        $stmt = $db->Prepare($sql);
        $status =  $stmt->execute(array($id));
        if ($status){
            $_SESSION["Message"] = "<div style='color: green;font-weight: bold;font-size: 20px;'>Deleted successfully.</div>";
            header("location: view_ajaxTeacher.php");
        }else{
            $_SESSION["Message"] = "failed to delet.";
        }
    }

    public function getCourseAjax($id)
    {

            //  $pdo = Database::getInstance();
            $db = database::getInstance();
            $query = $db->prepare('SELECT * FROM teachers WHERE t_id = :id  AND status=0');
            $query->execute(array(':id' => $id));
        $teachers=$query->fetch();

        $sql=$db->prepare('SELECT SUM(course_credit) as assign_credit FROM course_teacher WHERE teacher_id=:id AND stat=0');
        $sql->execute(array(':id' => $id));
        $credit=$sql->fetch();
        $data=[
            'teacher'=>$teachers,
            'a_c'=>$credit,
        ];

        return $data;
    }
}