<footer id="footer-bottom" class="well text-center">

        <div class="copyrights ">Copyright &copy; LEBS CODING 2017 | <a href="#">lebscoding.com</a></div>
        <!-- End social_icons -->

</footer>

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
